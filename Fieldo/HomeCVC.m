//
//  HomeCVC.m
//  Fieldo
//
//  Created by Gagan Joshi on 2/20/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//



#import "HomeCVC.h"
#import "CollectionViewCell.h"
#import "AppDelegate.h"
#import "PersistentStore.h"
#import "NSString+HTML.h"
#import "Language.h"
#import "MBProgressHUD.h"
#import "SubProjectRecord.h"
#import "StoresVC.h"
#import "NotesVC.h"
#import "SettingVC.h"
#import "RatingIndexVC.h"
#import "ContactUsTVC.h"
#import "Reachability.h"
#import "LogVC.h"
#import "ProjectReportTVC.h"
#import "ProjectsVC.h"
#import <SplunkMint/SplunkMint.h>

#import "BIZPopupViewController.h"
#import "StartProjectPopUpVC.h"

#import <AVFoundation/AVFoundation.h>
@interface HomeCVC ()

@end

@implementation HomeCVC

{
    UIView *dimBackgroundView;
    UIView *popupView;
    CollectionViewCell *cell;
    NSTimer *timer;
    AVAudioPlayer *audioObject;
    int d;
    NSString *dontCalTimerStr;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)receivedRotate:(NSNotification *)notification
{
    //Obtaining the current device orientation
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if(UIDeviceOrientationIsPortrait(orientation))
    {
        return;
    }
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.collectionViewLayout invalidateLayout];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    if (timer.isValid) {
//        [timer invalidate];
//    }
//    timer = nil;

    dontCalTimerStr=@"";
    
     [self.view endEditing:YES];
    [self notificationCenterActions];
    
    [dimBackgroundView setHidden:YES];
    [popupView setHidden:YES];
    
     NSString *str_className=NSStringFromClass([self class]);
    [[Mint sharedInstance] leaveBreadcrumb:str_className];
    
    self.arrayHomeMenu=[[NSMutableArray alloc] init];
    self.arrayImages=[[NSMutableArray alloc] init];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:flow];
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"CONTENT"];
    self.collectionView.pagingEnabled=YES;
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    
    [flow setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    if ([UIScreen mainScreen].bounds.size.height>480)
    {
        flow.sectionInset =  UIEdgeInsetsMake(15, 40, 15, 40);
        flow.minimumLineSpacing = 15;
    }
    else
    {
        flow.sectionInset =UIEdgeInsetsMake(12, 52, 10, 52);
       flow.minimumLineSpacing = 5;
    }

    flow.itemSize = self.collectionView.frame.size;

    self.navigationController.navigationBar.translucent=NO;
    self.collectionView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]];
    
    [self reloadCollectionView];
}


//-(void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//     [self reloadCollectionView];
//}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    
    self.stringDay=[dateFormatter stringFromDate:[NSDate date]];
    self.stringDate=[[NSString stringWithFormat:@"%@",[NSDate date]] substringWithRange:NSMakeRange(8,2)];

    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.mainMenuVC.menuView.hidden=YES;
    appDelegate.mainMenuVC.buttonAdvertise.hidden=NO;
    

    NSLog(@"*******************!!!");
    
  
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuth:)
     name:@"toRefreshHomeView"
     object:nil];
    
}


- (void) receiveToggleAuth:(NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"toRefreshHomeView"]) {
        
        
    //    [self viewDidLoad];
        [self refreshWholeCollectionView];
      // [self reloadCollectionView];
        
        
        
    }
    
}



-(void)refreshWholeCollectionView
{
    
    dontCalTimerStr = @"timerUnused";
    
    NSString *str_className=NSStringFromClass([self class]);
    [[Mint sharedInstance] leaveBreadcrumb:str_className];
    
    self.arrayHomeMenu=[[NSMutableArray alloc] init];
    self.arrayImages=[[NSMutableArray alloc] init];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:flow];
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"CONTENT"];
    self.collectionView.pagingEnabled=YES;
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    
    [flow setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    if ([UIScreen mainScreen].bounds.size.height>480)
    {
        flow.sectionInset =  UIEdgeInsetsMake(15, 40, 15, 40);
        flow.minimumLineSpacing = 15;
    }
    else
    {
        flow.sectionInset =UIEdgeInsetsMake(12, 52, 10, 52);
        flow.minimumLineSpacing = 5;
    }
    
    flow.itemSize = self.collectionView.frame.size;
    
    self.navigationController.navigationBar.translucent=NO;
    self.collectionView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]];
    
    [self reloadCollectionView];

}




-(void)notificationCenterActions
{
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(ViewReceiveToggleAuthUINotification:)
     name:@"StartTimerAuthentication"
     object:nil];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(ViewReceiveToggleAuthUINotification:)
     name:@"StopTimerAuthentication"
     object:nil];
}



- (void) ViewReceiveToggleAuthUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"StartTimerAuthentication"]) {
        
        
        //cell.labelTimer.text = @"123";
      //   cell.labelStartOrStop.text = @"Stop";
        NSString *startOrStop = @"Stop";
        NSString *onTimer = @"TimerStart";
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       // [defaults setObject:cell.labelTimer.text forKey:@"timer"];
        [defaults setObject:startOrStop forKey:@"start&Stop"];
        [defaults setObject:onTimer forKey:@"timerStatus"];
        [defaults synchronize];
       
        
        [self reloadCollectionView];
        [self.collectionView reloadData];
        [self viewDidLoad];
        [self viewWillAppear:YES];
        
        

    }
    
    else if ([[notification name] isEqualToString:@"StopTimerAuthentication"])
    
    {

        
         NSString *startOrStop = @"Start";
        NSString *onTimer = @"TimerStop";
      //  cell.labelStartOrStop.text = @"Start";
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       // [defaults setObject:cell.labelTimer.text forKey:@"timer"];
        [defaults setObject:startOrStop forKey:@"start&Stop"];
        [defaults setObject:onTimer forKey:@"timerStatus"];
        [defaults synchronize];
        
    
     
      [self reloadCollectionView];
       [self.collectionView reloadData];
        [self viewDidLoad];
        [self viewWillAppear:YES];
        


    }
}

-(void)reloadCollectionView
{
    
    if ([self.arrayHomeMenu count])
    {
        [self.arrayHomeMenu removeAllObjects];
        [self.arrayImages removeAllObjects];
    }
  
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
    {
        [self.arrayImages addObject:@"imageProjects.png"];
        [self.arrayImages addObject:@"imageCalendar.png"];
        [self.arrayImages addObject:@"imageLog.png"];
      //  [self.arrayImages addObject:@"imageStore.png"];
        [self.arrayImages addObject:@""];
        [self.arrayImages addObject:@"imageNotes.png"];
        [self.arrayImages addObject:@"imageSettings.png"];
        
        [self.arrayHomeMenu addObject:[Language get:@"Projects" alter:@"!Projects"]];
        [self.arrayHomeMenu addObject:[Language get:@"Calendar" alter:@"!Calendar"]];
        [self.arrayHomeMenu addObject:[Language get:@"Log" alter:@"!Log"]];
      //  [self.arrayHomeMenu addObject:[Language get:@"Stores" alter:@"!Stores"]];
            [self.arrayHomeMenu addObject:[Language get:@"Project Timer" alter:@"!Project Timer"]];
        [self.arrayHomeMenu addObject:[Language get:@"Monthly Report" alter:@"!Monthly Report"]];
//        [self.arrayHomeMenu addObject:[Language get:@"Notes" alter:@"!Notes"]];
        [self.arrayHomeMenu addObject:[Language get:@"Settings" alter:@"!Settings"]];
        
        // self.title=[NSString stringWithFormat:@"Welcome,%@",[[PersistentStore getWorkerName] stringByConvertingHTMLToPlainText]];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"EEEE MMM dd, yyyy"];
        
        
        NSString *dateString = [format stringFromDate:[NSDate date]];
        self.title=dateString;
    }
    else
    {
        [self.arrayImages addObject:@"imageProjects.png"];
        [self.arrayImages addObject:@"imageRating.png"];
        [self.arrayImages addObject:@"imageInvoice.png"];
        [self.arrayImages addObject:@"imageCheckRating.png"];
        [self.arrayImages addObject:@"imageContactUs.png"];
        [self.arrayImages addObject:@"imageSettings.png"];
        
        
        [self.arrayHomeMenu addObject:[Language get:@"Projects" alter:@"!Projects"]];
        [self.arrayHomeMenu addObject:[Language get:@"Rating" alter:@"!Rating"]];
        [self.arrayHomeMenu addObject:[Language get:@"Invoices" alter:@"!Invoices"]];
        [self.arrayHomeMenu addObject:[Language get:@"Rating Index" alter:@"!Rating Index"]];
        [self.arrayHomeMenu addObject:[Language get:@"Contact Us" alter:@"!Contact Us"]];
        [self.arrayHomeMenu addObject:[Language get:@"Settings" alter:@"!Settings"]];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"MMM dd, yyyy HH:mm"];
        
        
        NSString *dateString = [format stringFromDate:[NSDate date]];
        self.title=dateString;
        
        // self.title=[NSString stringWithFormat:@"Welcome,%@",[[PersistentStore getWorkerName] stringByConvertingHTMLToPlainText]];
      
        
    }
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^
                   {
                       NSData* imageData;
                       
                       if (![[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
                       {
//                           imageData=[NSData dataWithContentsOfFile:@"logo.jpg"];
                           
                       }
                       else
                       {
                           imageData=[NSData dataWithContentsOfURL:[NSURL URLWithString:[PersistentStore getCustomerImageUrl]]];
                           
                       }
                       
                       dispatch_async(dispatch_get_main_queue(), ^
                                      {
                                          AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                                          appDelegate.mainMenuVC.imageViewProfile.backgroundColor = [UIColor clearColor];
                                          appDelegate.mainMenuVC.imageViewProfile.image=([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])?[UIImage imageWithData:imageData]:[UIImage imageNamed:@"logo.jpg"];
                                      });
                   });
    

    [self.collectionView reloadData];

}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)loadView
{
    UIView* view = [[UIView alloc] init];
    self.view = view;
}



#pragma mark CollectionViewDelegate starts
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.arrayHomeMenu count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   cell =(CollectionViewCell *) [self.collectionView dequeueReusableCellWithReuseIdentifier:@"CONTENT" forIndexPath:indexPath];
    cell.imageView.image= [UIImage imageNamed:self.arrayImages[indexPath.row]];
    cell.labelTitle.text=self.arrayHomeMenu[indexPath.row];
    
    if (indexPath.row==1 && [[PersistentStore getLoginStatus] isEqualToString:@"Worker"] )
    {
        cell.labelDate.text=self.stringDate;
        cell.labelDay.text=self.stringDay;
    }
    else if (indexPath.row==3 && [[PersistentStore getLoginStatus] isEqualToString:@"Worker"] )
    {
     //cell.labelTimer.text = @"00:00:00";
        cell.timerImageView.image = [UIImage imageNamed:@"imageTimer.png"];
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *startOrStop = [defaults objectForKey:@"start&Stop"];
        if ([startOrStop isEqualToString:@"Stop"]) {
            cell.labelStartOrStop.text = [Language get:@"Stop" alter:@"!Stop"];
            cell.labelStartOrStop.backgroundColor = [UIColor clearColor];
        }
        else{
            cell.labelStartOrStop.text = [Language get:@"Start" alter:@"!Start"];
            cell.labelStartOrStop.backgroundColor = [UIColor clearColor];

          //  cell.labelTimer.text = @"00:00:00";
        
        }
        
        
        
        
        if ([dontCalTimerStr isEqualToString:@""]) {
        
        
        NSString *timerOn =  [defaults objectForKey:@"timerStatus"];
        if ([timerOn isEqualToString:@"TimerStart"]) {
        
            d=0;
            timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(runSchecduleTask:) userInfo:nil repeats:YES];
        
            NSLog(@"timer start****");
        }
        else
        {
            
            
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                if (screenSize.height > 480.0f)
                {
                    cell.labelTimer.frame = CGRectMake(20, 67, 60, 20);

                }
                else
                {
                    
                    cell.labelTimer.frame = CGRectMake(10, 55, 60, 20);
                }
                
            }
            
            
            
            cell.labelTimer.text = @"00:00:00";
            cell.labelTimer.backgroundColor = [UIColor clearColor];
            NSLog(@"timer stop****");
            if (timer.isValid) {
                [timer invalidate];
            }
            timer = nil;
        }
 
        
      
        }
        
        else
        {
            dontCalTimerStr=@"";
            
            NSString *timerOn =  [defaults objectForKey:@"timerStatus"];
            if ([timerOn isEqualToString:@"TimerStart"]) {

            }
            else
            {
                
                CGSize screenSize = [[UIScreen mainScreen] bounds].size;
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    if (screenSize.height > 480.0f)
                    {
                        cell.labelTimer.frame = CGRectMake(20, 67, 60, 20);
                        
                    }
                    else
                    {
                        
                        cell.labelTimer.frame = CGRectMake(10, 55, 60, 20);
                    }
                    
                }

                cell.labelTimer.text = @"00:00:00";
                cell.labelTimer.backgroundColor = [UIColor clearColor];
                NSLog(@"timer stop****");
                if (timer.isValid) {
                    [timer invalidate];
                }
                timer = nil;
            }

        }
        
       
        
     
    }
    else
    {
        cell.labelDate.text=@"";
        cell.labelDay.text=@"";
        //cell.labelTimer.text = @"3333";
        cell.labelStartOrStop.text = @"";
    }
    
    
    
    return cell;
}





- (void)runSchecduleTask:(NSTimer *) runningTimer{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *timerOn =  [defaults objectForKey:@"timerStatus"];
    
    if ([timerOn isEqualToString:@"TimerStop"]) {
        cell.labelTimer.text=@"";
        cell.labelTimer.backgroundColor = [UIColor clearColor];
        if (timer.isValid) {
            [timer invalidate];
        }
        timer = nil;
        d=0;
        //NSLog(@"timer stoped****");

    }
    else
    {
          // NSLog(@"timer started****");
        
        int minutes, seconds,hours ;
        d++;
        //NSLog(@"d=%d",d);
        hours= d / 3600;
        minutes = (d % 3600) / 60;
        seconds= (d %3600) % 60;
        cell.labelTimer.text=[NSString stringWithFormat:@"%02d:%02d:%02d", hours,minutes, seconds];

      //  NSLog(@"seconds%d",seconds);
    if (minutes==59 && seconds==59) {
        
          //  NSLog(@"hey beeep******");
           [self avAudioAction];
            
        }
        
//        else if (seconds==59) {
//           // NSLog(@"hey beeep******");
//            [self avAudioAction];
//            
//        }
        
        
        
       
        
    }
    

    
}



-(void)avAudioAction{
    NSURL *urlObject=[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"beep" ofType:@"mp3"]];
    
    audioObject=[[AVAudioPlayer alloc]initWithContentsOfURL:urlObject error:nil];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    audioObject.volume=1;
    [audioObject play];
    
}


//-(NSString *)sendTime:(NSString*)seconds min:(NSString*)minutes hour:(NSString*)hours
//{
//    
//    if ([seconds intValue]<10) {
//        seconds = [NSString stringWithFormat:@"%s%@","0",seconds];
//    }
//    else if ([minutes intValue]<10) {
//        minutes = [NSString stringWithFormat:@"%s%@","0",minutes];
//    }
//    else if ([hours intValue]<10) {
//        hours = [NSString stringWithFormat:@"%s%@","0",hours];
//    }
//    return seconds,minutes,hours;
//}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (screenSize.height > 480.0f)
            return CGSizeMake(100, 120);
    }
    return CGSizeMake(80, 100);
}


-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"index %ld",(long)indexPath.row);
    CGRect rect= CGRectMake(0, 100, 320, 380);
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (screenSize.height > 480.0f)
            rect=CGRectMake(0, 100, 320, 468);
    }
    
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.mainMenuVC.menuView.hidden=NO;
    appDelegate.mainMenuVC.buttonAdvertise.hidden=YES;

    
    APP_DELEGATE.checkLogView = NO;
    
    switch (indexPath.row) {
        case 0:
        {
            
            
            [appDelegate.mainMenuVC.btnProject setBackgroundImage:[UIImage imageNamed:@"SelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnProject setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [appDelegate.mainMenuVC.btnCalOrRating setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnCalOrRating setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            
            [appDelegate.mainMenuVC.btnLogOrInvoice setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnLogOrInvoice setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            
            [appDelegate.mainMenuVC.btnHome setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnHome setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            NSLog(@"%@",appDelegate.mainMenuVC.contentView);
            if([appDelegate.mainMenuVC.contentView.subviews count] == 1)
            {
                [[appDelegate.mainMenuVC.contentView.subviews objectAtIndex:0] removeFromSuperview];
            }
            UIViewController* controller = (UIViewController*)[appDelegate.mainMenuVC.childViewControllers objectAtIndex:0];
            controller.view.frame = appDelegate.mainMenuVC.contentView.bounds;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ToProjectsVC" object:self userInfo:nil];

            [appDelegate.mainMenuVC.contentView addSubview:controller.view];
            

            
            break;
        }
        case 1:
        {
            [appDelegate.mainMenuVC.btnProject setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnProject setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            [appDelegate.mainMenuVC.btnCalOrRating setBackgroundImage:[UIImage imageNamed:@"SelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnCalOrRating setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [appDelegate.mainMenuVC.btnLogOrInvoice setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnLogOrInvoice setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            
            [appDelegate.mainMenuVC.btnHome setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnHome setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            [PersistentStore setFlagLog:@"YES"];
            NSLog(@"%@",appDelegate.mainMenuVC.contentView);
            if([appDelegate.mainMenuVC.contentView.subviews count] == 1)
            {
                [[appDelegate.mainMenuVC.contentView.subviews objectAtIndex:0] removeFromSuperview];
            }
            UIViewController* controller = (UIViewController*)[appDelegate.mainMenuVC.childViewControllers objectAtIndex:1];
            controller.view.frame = appDelegate.mainMenuVC.contentView.bounds;

            [appDelegate.mainMenuVC.contentView addSubview:controller.view];
            break;

        }
        case 2:
        {
            [appDelegate.mainMenuVC.btnProject setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnProject setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            [appDelegate.mainMenuVC.btnCalOrRating setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnCalOrRating setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            [appDelegate.mainMenuVC.btnLogOrInvoice setBackgroundImage:[UIImage imageNamed:@"SelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnLogOrInvoice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [appDelegate.mainMenuVC.btnHome setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
            [appDelegate.mainMenuVC.btnHome setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
            
            
            [PersistentStore setFlagLog:@"YES"];
            NSLog(@"%@",appDelegate.mainMenuVC.contentView);
            if([appDelegate.mainMenuVC.contentView.subviews count] == 1)
            {
                [[appDelegate.mainMenuVC.contentView.subviews objectAtIndex:0] removeFromSuperview];
            }
            UIViewController* controller = (UIViewController*)[appDelegate.mainMenuVC.childViewControllers objectAtIndex:2];
            controller.view.frame = appDelegate.mainMenuVC.contentView.bounds;
            
            if([controller isKindOfClass:[LogVC class]])
            {
                LogVC *logVC = (LogVC*)controller;
                logVC.shouldSelectProjectBtn = NO;
            } else if([controller isKindOfClass:[UINavigationController class]]) {
                if ([[(UINavigationController*)controller topViewController] isKindOfClass:[LogVC class]]) {
                    LogVC *logVC = (LogVC*)[(UINavigationController*)controller topViewController];
                    logVC.shouldSelectProjectBtn = YES;
                }
            }
            [appDelegate.mainMenuVC.contentView addSubview:controller.view];
            break;

           
        }
        case 3:
        {
            
            if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
            {
                
                
               // StoresVC *storesVC=[[StoresVC alloc] init];
               // [self.navigationController pushViewController:storesVC animated:YES];

                
//                dimBackgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//                dimBackgroundView.backgroundColor = [UIColor lightGrayColor];
//                 [dimBackgroundView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];
//                [self.collectionView addSubview:dimBackgroundView];
//                
//                
//                popupView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 250)];
//                popupView.center = CGPointMake(self.view.frame.size.width  / 2,self.view.frame.size.height / 2);
//                popupView.backgroundColor = [UIColor whiteColor];
//                popupView.layer.borderWidth = 0.6;
//                popupView.layer.cornerRadius = 7.0;
//                popupView.clipsToBounds=YES;
//                popupView.layer.borderColor = [UIColor blackColor].CGColor;
//                [dimBackgroundView addSubview:popupView];
//                
//                UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 20)];
//                titleLabel.text =@"Title";
//                titleLabel.center = CGPointMake(popupView.frame.size.width  / 2,30);
//                titleLabel.textAlignment = NSTextAlignmentCenter;
//                titleLabel.backgroundColor = [UIColor clearColor];
//                titleLabel.textColor = [UIColor blackColor];
//                [popupView addSubview:titleLabel];
//                
//                UIButton *startButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 200, 30)];
//                startButton.backgroundColor = [UIColor grayColor];
//                startButton.center = CGPointMake(popupView.frame.size.width  / 2,220);
//                [startButton setTitle:@"Start" forState:UIControlStateNormal];
//                [startButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
//                [startButton addTarget:self action:@selector(hideBackgroundAndPopupViewAction) forControlEvents:UIControlEventTouchUpInside];
//                [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                [popupView addSubview:startButton];
//                
//                
//                [dimBackgroundView setHidden:NO];
//                [popupView setHidden:NO];
                
                
                //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                //PopUpVC *smallViewController = [storyboard instantiateViewControllerWithIdentifier:@"bigViewController"];
                //smallViewController.eventIdString = eventIdString;
                
                
                
                
                if (APP_DELEGATE.isServerReachable) {
                
                
                
                
                StartProjectPopUpVC *smallViewController=[[StartProjectPopUpVC alloc]initWithNibName:@"StartProjectPopUpVC" bundle:nil];
                //[self.navigationController pushViewController:smallViewController animated:YES];
                
                BIZPopupViewController *popupViewController;
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                int countValue = (int)[defaults integerForKey:@"count"];
                
                if (countValue==1) {
                
                popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:smallViewController contentSize:CGSizeMake(280, 300)];
                }
                else
                {
                     popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:smallViewController contentSize:CGSizeMake(280, 220)];
                }
                [self presentViewController:popupViewController animated:NO completion:nil];

                
                NSLog(@"&&&&&&&&&&&&&&&");
                    
               
                }
                
                else
                {
                    [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

                }
                
                     break;
  
            }
            else
            {
                RatingIndexVC *ratingIndexVC=[[RatingIndexVC alloc] init];
                [self.navigationController pushViewController:ratingIndexVC animated:YES];

                break;
            }
            
          
            
        }
        case 4:
        {
            if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
            {
//                NotesVC *notesVC=[[NotesVC alloc] init];
//                [self.navigationController pushViewController:notesVC animated:YES];
                ProjectReportTVC *projectReportTVC=[[ProjectReportTVC alloc] initWithNibName:nil bundle:nil];
                projectReportTVC.stringWorkerId=[PersistentStore getWorkerID];
//                projectReportTVC.stringHeadId=SubProject.headId;
                projectReportTVC.isSetting=YES;
                [self.navigationController pushViewController:projectReportTVC animated:YES];
                [PersistentStore setFlagLog:@"NO"];
                break;

            }
            else
            {
               
                ContactUsTVC *contactUsTVC=[[ContactUsTVC alloc] init];
                [self.navigationController pushViewController:contactUsTVC animated:YES];
                break;

            }
         }
        case 5:
        {
            
            SettingVC *settingVC=[[SettingVC alloc] init];
            [self.navigationController pushViewController:settingVC animated:YES];
            break;
            
           
        }
           default:
            break;
    }
    
    
}





//-(void)countDown:(NSTimer *) aTimer {
//   cell.labelTimer.text = [NSString stringWithFormat:@"%d",[cell.labelTimer.text  intValue] - 1];
//    if ([cell.labelTimer.text isEqualToString:@"0"]) {
//        //do whatever
//        [aTimer invalidate];
//    }
//}




-(void)hideBackgroundAndPopupViewAction
{
    [dimBackgroundView setHidden:YES];
    [popupView setHidden:YES];
    
}









- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
