//
//  WorkPlanVC.h
//  Fieldo
//
//  Created by Gagan Joshi on 11/19/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkPlanVC : UITableViewController
{
    NSString *stringProjectId;
    NSString *stringProjectExtId;
    NSString *stringAddress;

}


@property (nonatomic,strong) NSMutableArray *arrayWork;

@property(nonatomic,retain) NSString *stringProjectId;
@property(nonatomic,retain) NSString *stringProjectExtId;
@property(nonatomic,retain) NSString *stringAddress;

@property(nonatomic,retain) NSString *stringProjectName;

@property (nonatomic,strong) NSMutableArray *arrayIndexExpandedSections;

@property (nonatomic,strong) UIProgressView *progressView;
@property(nonatomic,retain) NSString *strProjectComplete;
@property(nonatomic,retain) UILabel *lblProgressView;






@end
