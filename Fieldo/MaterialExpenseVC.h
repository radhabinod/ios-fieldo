//
//  MaterialExpenseVC.h
//  Fieldo
//
//  Created by Gagan Joshi on 11/12/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Base64.h"

@interface MaterialExpenseVC : UITableViewController<UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    UIImagePickerController* picker;
}
@property (nonatomic,retain) UIImage *imageMaterial;

@property(nonatomic,retain) NSString *stringProjectId;
@property(nonatomic,retain)  NSString *stringProjectExtId;
@property(nonatomic,retain) NSString *stringProjectName;
@property(nonatomic,retain)  NSString *stringLocation;
@property(nonatomic,retain)  NSString *stringCity;
@property(nonatomic,retain)  NSString *stringState;
@property(nonatomic,retain)  NSString *stringCountry;
@property(nonatomic,retain) NSString *stringHeadId;
@property(nonatomic,retain) NSString *taskName;
@property (nonatomic,strong) NSMutableArray *arrayProjects;
@property(nonatomic,strong)  NSMutableArray *arrayStores;


//Mandeep
// Edit log
@property(nonatomic,retain) NSString *date;
@property(nonatomic,retain) NSString *comment;
@property(nonatomic,retain) NSString *material_amt;
@property(nonatomic,retain) NSString *material_id;
@property(nonatomic,retain) NSString *material_name;
@property(nonatomic,retain) NSString *order_no;
@property(nonatomic,retain) NSString *order_value;
@property(nonatomic,retain) NSString *project_id;



@end
