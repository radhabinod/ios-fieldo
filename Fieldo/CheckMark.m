//
//  CheckMark.m
//  Fieldo
//
//  Created by Parveen Sharma on 8/12/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import "CheckMark.h"

@implementation CheckMark
@synthesize checkBtn,vehicle,button_lunch,button_vehicle;

- (void)awakeFromNib {
    // Initialization code
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)SelectVehicle:(id)sender{
    checkBtn=(UIButton*) sender;
    checkBtn.selected=!checkBtn.selected;
    if (checkBtn.selected) {
        
        [checkBtn setImage:[UIImage imageNamed:@"tick_blue.png"] forState:UIControlStateNormal];
    }
    else{
        [checkBtn setImage:[UIImage imageNamed:@"f-circle_128.png"] forState:UIControlStateNormal];

    }
}

- (IBAction)SelectLunch:(UIButton *)sender {
    checkBtn=(UIButton*) sender;
    checkBtn.selected=!checkBtn.selected;
    
    NSString *str_Checked = @"";
    
    if (checkBtn.selected) {
        [checkBtn setImage:[UIImage imageNamed:@"tick_blue.png"] forState:UIControlStateNormal];
        str_Checked = @"Yes";
        
    }
    else{
        [checkBtn setImage:[UIImage imageNamed:@"f-circle_128.png"] forState:UIControlStateNormal];
        str_Checked = @"No";
        
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:str_Checked forKey:@"Status"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadTimeLogTable" object:nil userInfo:dict];
}


@end
