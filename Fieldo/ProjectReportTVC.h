//
//  ProjectReportTVC.h
//  Fieldo
//
//  Created by Gagan Joshi on 11/28/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectReportTVC : UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource>
{
    UILabel *labelData;
}
@property(nonatomic,retain) NSString *stringProjectId;
@property(nonatomic,retain)  NSString *stringProjectExtId;
@property(nonatomic,retain) NSString *stringHeadId;

@property(nonatomic,retain)  NSString *stringLocation;
@property(nonatomic,retain)  NSString *stringCity;
@property(nonatomic,retain)  NSString *stringState;
@property(nonatomic,retain)  NSString *stringCountry;
@property(nonatomic,retain) NSString *stringProjectName;
@property(nonatomic,retain) NSString *stringWorkerId;

@property(strong,nonatomic) NSMutableArray *arrayTime;
@property(nonatomic,retain) NSMutableArray *arrayMaterial;
@property(nonatomic,retain) NSMutableArray *arrayTravel;

@property(nonatomic,retain) UISegmentedControl *segmentControl;

@property (nonatomic, assign) BOOL isSetting;

@end