 //
//  ProjectReportTVC.m
//  Fieldo
//
//  Created by Gagan Joshi on 11/28/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "ProjectReportTVC.h"
#import "MBProgressHUD.h"
#import "Language.h"
#import "PersistentStore.h"
#import "NSString+HTML.h"
#import "TimeLogVC.h"
#import "MaterialExpenseVC.h"
#import "TravelExpenseVC.h"
#import <SplunkMint/SplunkMint.h>
@interface ProjectReportTVC ()
{
//    NSArray *monthArray;
    NSMutableArray *monthArray;
    UIPickerView *monthPicker;
    UIToolbar *toolBar;
    
    int monthNumber;
    int row1;
    UIButton *selectMonth;
    NSString *totalWorkingHours;
}

@end

@implementation ProjectReportTVC
@synthesize isSetting;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        row1=0;
        totalWorkingHours=@"0";
        // Custom initialization
    }
    return self;
}

- (void) postRequestProjectReports
{
    if (self.stringProjectId == nil && !isSetting)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Fieldo" message:[Language get:@"No project lists are available." alter:@"!No project lists are available."]  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        alert.tag = 1;
        
        [alert show];
    }
    else
    {
     [self showLoadingView];
    
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    [postDict setObject:self.stringWorkerId forKey:@"worker_id"];
    if (!isSetting) {
        [postDict setObject:self.stringProjectId forKey:@"project_id"];
        if (![self.stringHeadId isEqualToString:@"0"]) {
            [postDict setObject:self.stringHeadId forKey:@"head_id"];
        }
    }
    else
    {
        [postDict setObject:[NSString stringWithFormat:@"%i",monthNumber] forKey:@"selmonth"];
    }
    
    if (APP_DELEGATE.isServerReachable) {
    NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
    NSMutableURLRequest *urlRequest;
    if (isSetting) {
            urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://fieldo.se/api/getmonthlyreport.php"]];
    }
    else
    {
            urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:([self.stringHeadId isEqualToString:@"0"])?@"http://fieldo.se/api/proreport.php":@"http://fieldo.se/api/16july2015_proreport.php"]];
    }
    [urlRequest setTimeoutInterval:180];
    NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
    [urlRequest setHTTPMethod:@"POST"];
    
        NSLog(@"urlRequest: %@ postDict: %@",urlRequest,postDict);
        
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
         NSLog(@"%@",object);
         if (error)
         {
             NSLog(@"Error: %@",[error description]);
         }
         if ([object isKindOfClass:[NSDictionary class]] == YES)
         {
             if ([object[@"CODE"] intValue]==1)
             {
                  [self performSelectorOnMainThread:@selector(alertLoginFailed) withObject:nil waitUntilDone:YES];
             }
             else
             {
                 [labelData removeFromSuperview];
                 
                 if ([self.arrayTime isKindOfClass:[NSArray class]])
                 {
                    [self performSelectorOnMainThread:@selector(noData) withObject:nil waitUntilDone:YES];
                 }
                 else if (self.arrayMaterial == nil)
                 {
                     [self performSelectorOnMainThread:@selector(noData) withObject:nil waitUntilDone:YES];
                 }
                 else if (self.arrayTravel == nil)
                 {
                     [self performSelectorOnMainThread:@selector(noData) withObject:nil waitUntilDone:YES];
                 }
                 
                 if (!isSetting) {
                     self.arrayMaterial=object[@"data"][@"material"];
                     self.arrayTravel=object[@"data"][@"KM"];
                     self.arrayTime =object[@"data"][@"TimeLog"];
                 }
                 else
                 {
                     self.arrayMaterial=object[@"data"][@"MaterialLog"];
                     self.arrayTravel=object[@"data"][@"TravelLog"];
                     self.arrayTime =object[@"data"][@"TimeLog"];
                 }
                 totalWorkingHours=object[@"total_wrkng_hrs"];
                 
                [self performSelectorOnMainThread:@selector(refreshTable) withObject:nil waitUntilDone:YES];
             }
         }
     }];
    }
        
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
}
- (void) noData
{
    labelData = [[UILabel alloc] initWithFrame:CGRectMake(125, 185, 100, 30)];
    
    labelData.text = @"No Data.";
    
    labelData.textColor = [UIColor lightGrayColor];
    
    [self.tableView addSubview:labelData];
}

- (void) alertLoginFailed
{
    [self hideLoadingView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fieldo" message:[Language get:@"No data found." alter:@"!No data found."] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 1;
    
    [alert show];
}

#pragma mark - UIAlertView Delegate

- (void) alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger) buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showLoadingView
{
    //self.tableView.hidden=YES;
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading...";
    hud.dimBackground = YES;
}

-(void)refreshTable
{
    [self hideLoadingView];
}

-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.tableView reloadData];
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    
    NSLog(@"Location : %@", self.stringLocation);

    if (!isSetting) {
        [self postRequestProjectReports];
    }
    else
    {
        for (int i=0; i<[monthArray count]; i++) {
            
            NSDate *monthDisplay = [NSDate date];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"MM"];
            NSString *stringMonth = [dateFormat stringFromDate:monthDisplay];
            NSLog(@"stringMonth : %@",stringMonth);
            int MonthNumber=[stringMonth intValue];
       
            if ([[[monthArray objectAtIndex:i] valueForKey:@"monthNumber"] intValue]==MonthNumber) {
                
                // Current Month
                
                monthNumber = [[[monthArray objectAtIndex:i] valueForKey:@"monthNumber"] intValue];
                row1 = (int)i;
              
            }
         
        }
        
        [self postRequestProjectReports];
    }
//    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    
     NSString *str_className=NSStringFromClass([self class]);
    [[Mint sharedInstance] leaveBreadcrumb:str_className];
    
    [super viewDidLoad];
    
    int startYear = 0;
    int endYear = 0;

    int startMonth=0;
    int endMonth=0;
    
    
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate * currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    [comps setYear:0];
    NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    [comps setYear: -1];
    NSDate * minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
   
    
    
    NSLog(@"maxDate : %@",maxDate);
     NSLog(@"minDate : %@",minDate);
    
    NSDateFormatter *DateFormatterYear=[[NSDateFormatter alloc] init];
    [DateFormatterYear setDateFormat:@"yyyy"];
    
    NSString *str_minDateYear = [DateFormatterYear stringFromDate:minDate];
    NSString *str_maxDateYear = [DateFormatterYear stringFromDate:maxDate];
    
    startYear=[str_minDateYear intValue];
    endYear=[str_maxDateYear intValue];
    
    NSDateFormatter *DateFormatterMonth=[[NSDateFormatter alloc] init];
    [DateFormatterMonth setDateFormat:@"MM"];
    
    NSString *str_minDateMonth = [DateFormatterMonth stringFromDate:minDate];
    NSString *str_maxDateMonth = [DateFormatterMonth stringFromDate:maxDate];
    
    startMonth=[str_minDateMonth intValue];
    endMonth=[str_maxDateMonth intValue];
    
    
//    monthArray = @[[Language get:@"January" alter:@"!January"], [Language get:@"Febuary" alter:@"!Febuary"], [Language get:@"March" alter:@"!March"], [Language get:@"April" alter:@"!April"], [Language get:@"May" alter:@"!May"], [Language get:@"June" alter:@"!June"], [Language get:@"July" alter:@"!July"], [Language get:@"August" alter:@"!August"], [Language get:@"September" alter:@"!September"], [Language get:@"October" alter:@"!October"], [Language get:@"November" alter:@"!November"], [Language get:@"December" alter:@"!December"]];

    monthArray=[[NSMutableArray alloc] init];
    
    for (int i=startYear; i<=endYear; i++) {
        
        
        if (i==endYear) {
         
            for (int j=1; j<=endMonth; j++) {
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                
                if (j==1) {
                    
                    [dict setValue:[Language get:@"January" alter:@"!January"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==2) {
                    
                    [dict setValue:[Language get:@"February" alter:@"!February"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==3) {
                    
                    [dict setValue:[Language get:@"March" alter:@"!March"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==4) {
                    
                    [dict setValue:[Language get:@"April" alter:@"!April"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==5) {
                    
                    [dict setValue:[Language get:@"May" alter:@"!May"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==6) {
                    
                    [dict setValue:[Language get:@"June" alter:@"!June"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==7) {
                    
                    [dict setValue:[Language get:@"July" alter:@"!July"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==8) {
                    
                    [dict setValue:[Language get:@"August" alter:@"!August"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==9) {
                    
                    [dict setValue:[Language get:@"September" alter:@"!September"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==10) {
                    
                    [dict setValue:[Language get:@"October" alter:@"!October"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==11) {
                    
                    [dict setValue:[Language get:@"November" alter:@"!November"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==12) {
                    
                    [dict setValue:[Language get:@"December" alter:@"!December"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                
                [monthArray addObject:dict];
                
            }
            
        }
        else
        {
            for (int j=1; j<=12; j++) {
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                
                if (j>endMonth&&j!=endMonth){
                    
                if (j==1) {
                    
                    [dict setValue:[Language get:@"January" alter:@"!January"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==2) {
                    
                    [dict setValue:[Language get:@"February" alter:@"!February"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==3) {
                    
                    [dict setValue:[Language get:@"March" alter:@"!March"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==4) {
                    
                    [dict setValue:[Language get:@"April" alter:@"!April"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==5) {
                    
                    [dict setValue:[Language get:@"May" alter:@"!May"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==6) {
                    
                    [dict setValue:[Language get:@"June" alter:@"!June"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==7) {
                    
                    [dict setValue:[Language get:@"July" alter:@"!July"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==8) {
                    
                    [dict setValue:[Language get:@"August" alter:@"!August"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==9) {
                    
                    [dict setValue:[Language get:@"September" alter:@"!September"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==10) {
                    
                    [dict setValue:[Language get:@"October" alter:@"!October"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==11) {
                    
                    [dict setValue:[Language get:@"November" alter:@"!November"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                else if (j==12) {
                    
                    [dict setValue:[Language get:@"December" alter:@"!December"] forKey:@"month"];
                    [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"year"];
                    [dict setValue:[NSString stringWithFormat:@"%d",j] forKey:@"monthNumber"];
                }
                
                [monthArray addObject:dict];
                
            }
            }
        }
        
        
    }
    
    
    NSLog(@"monthArray : %@",monthArray);
    
    
    
//    @ "January", @ "Februar", @ "Mars", @ "April", @ "May", @ "June", @ "July", @ "August", @ "September", @ "October", @ "november", @ "desember"
    self.arrayTime     =   [[NSMutableArray alloc] init];
    self.arrayTravel   =   [[NSMutableArray alloc] init];
    self.arrayMaterial =   [[NSMutableArray alloc] init];
    
    self.title=(isSetting)?[Language get:@"Monthly Report" alter:@"!Monthly Report"]:[Language get:@"Project Report" alter:@"!Project Report"]  ;
    self.tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"] ];
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    NSMutableArray *array=[[NSMutableArray alloc] init];
    [array addObject:[Language get:@"Time Log" alter:@"!Time Log"]];
    [array addObject:[Language get:@"Material Log" alter:@"!Material Log"]];
    [array addObject:[Language get:@"Travel Log" alter:@"!Travel Log"]];
    
    self.segmentControl = [[UISegmentedControl alloc]initWithItems:array];
    self.segmentControl.frame = CGRectMake(10, 7, 300, 30);
    [self.segmentControl addTarget:self action:@selector(segmentedControl_ValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.segmentControl setSelectedSegmentIndex:0];
    [view addSubview:self.segmentControl];
      self.tableView.tableHeaderView = view;
    
    monthPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 750, 320, 162)];
    monthPicker.delegate = self;
    monthPicker.dataSource = self;
    monthPicker.backgroundColor = [UIColor lightGrayColor];
    [APP_DELEGATE.window addSubview:monthPicker];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 700, 320, 44)];
    toolBar.backgroundColor = [UIColor clearColor];
    toolBar.barStyle = UIBarStyleBlackTranslucent;

    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButton)];
    UIBarButtonItem *flexibleButton =[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    NSArray *arrayDone = [NSArray arrayWithObjects:flexibleButton,doneBtn, nil];
    [toolBar setItems:arrayDone];
    
    [APP_DELEGATE.window addSubview:toolBar];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 30, 30); // custom frame
    [backButton setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goToPrevious) forControlEvents:UIControlEventTouchUpInside];
    
    // set left barButtonItem to backButton
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
}

-(void)goToPrevious
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)segmentedControl_ValueChanged:(UISegmentedControl *)segment
{
    
    if(segment.selectedSegmentIndex == 0)
    {
        //action for the first button (Current)
    }
    if(segment.selectedSegmentIndex == 1)
    {
        //action for the first button (Current)
    }
    if (segment.selectedSegmentIndex == 2)
    {
        
    }
    
    [self.tableView reloadData];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self doneButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
  if(self.segmentControl.selectedSegmentIndex ==0)
  {
      return [self.arrayTime count];
  }
  else if (self.segmentControl.selectedSegmentIndex ==1)
  {
        return [self.arrayMaterial count];
  }
  else
  {
      if ([self.arrayTravel isKindOfClass:[NSArray class]])
      {
          labelData.hidden = NO;
          return [self.arrayTravel count];  
      }
      else
      {
          return [self.arrayTravel count];
      }
 }
    
    return NO;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(self.segmentControl.selectedSegmentIndex ==0)
//        return 0;
    return 100;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        // 1: To provide feedback to the user, create a UIActivityIndicatorView and set it as the cellís accessory view.
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(self.segmentControl.selectedSegmentIndex ==0)
    {
        
    
        /*
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//      [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//      [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        
        NSMutableArray *arrayDate = [self.arrayTime valueForKey:@"date"];
        NSDate *date123=[dateFormatter dateFromString:[NSString stringWithFormat:@"%@", arrayDate]];
        
        NSDateFormatter *dateFormatter2=[[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd-MMMM-YYYY"];
        
//      [self.arrayTime addObject:[dateFormatter2 dateFromString:[NSString stringWithFormat:@"%@", date123]]];
        NSMutableArray *arrayDateTime = [[NSMutableArray alloc] init];
        NSMutableArray *array = [self.arrayTime valueForKey:@"date"];
        for (NSDate* dateAsDate in array)
        {
           //get the date next NSDate from the array as: NSDate *dateAsDate...
           NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
           [dateFormat setDateFormat:@"dd-MMMM-yyyy"];
           NSString *dateAsStr = [dateFormat stringFromDate:date123];
           [arrayDateTime addObject:dateAsStr];
        }
        */
        
        
        NSString *str = [[self.arrayTime objectAtIndex:indexPath.row] valueForKey:@"date"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *date = [dateFormatter dateFromString:str];
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *newDateString = [dateFormatter2 stringFromDate:date];
        
        NSString *DateStr=[NSString stringWithFormat:@"%@ - %@ Hours",newDateString,self.arrayTime[indexPath.row][@"tdiff"]]
        ;
        NSString *string=[NSString stringWithFormat:@"[%@]-%@",self.arrayTime[indexPath.row][@"project_id"],self.arrayTime[indexPath.row][@"title"]];

        cell.textLabel.text = [NSString stringWithFormat:@"%@", [string stringByConvertingHTMLToPlainText]];

        NSString *vehicleused=[self.arrayTime[indexPath.row][@"is_vehicle_used"]boolValue]?@"YES":@"NO";
        NSString *finalDetailStr=[NSString stringWithFormat:@"%@\n%@\n%@ %@",[DateStr stringByConvertingHTMLToPlainText],[self.arrayTime[indexPath.row][@"comment"] stringByConvertingHTMLToPlainText],[Language get:@"Vehicle :" alter:@"!Vehicle :"],vehicleused];
        cell.detailTextLabel.numberOfLines=0;
        cell.detailTextLabel.text=finalDetailStr ;
        
        cell.textLabel.font = [UIFont systemFontOfSize:12];

        labelData.hidden = YES;
        
        
        UIButton *button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame=CGRectMake(0,0,55,22);
        [button setTitle:[Language get:@"Edit" alter:@"!Edit"] forState:UIControlStateNormal];
        button.layer.cornerRadius = 3;
        button.layer.borderColor = [[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] CGColor];
        button.layer.borderWidth = .8f;
        [button addTarget:self action:@selector(action_EditTimeReport:) forControlEvents:UIControlEventTouchUpInside];
        button.titleLabel.font=[UIFont systemFontOfSize:10];
        // [cell.contentView addSubview:button];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryView=button;
        
        
        
        
    }
    else if (self.segmentControl.selectedSegmentIndex ==1)
    {
        
        NSString *materialName=[[NSString stringWithFormat:@"%@",self.arrayMaterial[indexPath.row][@"material_name"]] stringByConvertingHTMLToPlainText];
        NSString *OrderNo=[NSString stringWithFormat:@"Order No. - %@",self.arrayMaterial[indexPath.row][@"order_no"]];
        NSString *OrderValue=([[NSString stringWithFormat:@"%@",self.arrayMaterial[indexPath.row][@"order_value"]] length]>0)?[NSString stringWithFormat:@"Order Value - %@",self.arrayMaterial[indexPath.row][@"order_value"]]:@"";
//      NSString *OrderDate=[NSString stringWithFormat:@"%@",self.arrayMaterial[indexPath.row][@"date"]];
        
        NSString *OrderDate = [[self.arrayMaterial objectAtIndex:indexPath.row] valueForKey:@"date"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *date = [dateFormatter dateFromString:OrderDate];
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *newDateString = [dateFormatter2 stringFromDate:date];
        cell.textLabel.numberOfLines=0;

        cell.textLabel.text=[NSString stringWithFormat:@"%@    %@\n%@    %@",materialName,newDateString,OrderNo,OrderValue];
        
        NSString *string=[NSString stringWithFormat:@"[%@]-%@",self.arrayMaterial[indexPath.row][@"project_id"],self.arrayMaterial[indexPath.row][@"title"]];
        
        NSString *finalDetailStr=[NSString stringWithFormat:@"%@\n%@",[string stringByConvertingHTMLToPlainText],[self.arrayMaterial[indexPath.row][@"comment"] stringByConvertingHTMLToPlainText]];
        cell.detailTextLabel.numberOfLines=0;
        cell.detailTextLabel.text=finalDetailStr ;
        
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        
        labelData.hidden = YES;
        
        UIButton *button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame=CGRectMake(0,0,55,22);
        [button setTitle:[Language get:@"Edit" alter:@"!Edit"] forState:UIControlStateNormal];
        button.layer.cornerRadius = 3;
        button.layer.borderColor = [[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] CGColor];
        button.layer.borderWidth = .8f;
        [button addTarget:self action:@selector(action_EditMaterialReport:) forControlEvents:UIControlEventTouchUpInside];
        button.titleLabel.font=[UIFont systemFontOfSize:10];
        // [cell.contentView addSubview:button];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryView=button;
        
        
        
    }
    else
    {
        
        NSString *resourceparams = [NSString stringWithFormat:@"%@",self.arrayTravel[indexPath.row][@"from"]];
        NSLog(@"before replace resourceparams: %@", resourceparams);
        resourceparams = [resourceparams stringByReplacingOccurrencesOfString:@"\\U" withString:@"\\u"];
        NSLog(@"after replace resourceparams: %@", resourceparams);
        
         NSString *string=[NSString stringWithFormat:@"%@ - %@\n%@, %@",self.arrayTravel[indexPath.row][@"from"],self.arrayTravel[indexPath.row][@"to"],self.arrayTravel[indexPath.row][@"km"],self.arrayTravel[indexPath.row][@"other_fee"]];
        

        NSString *str = [[self.arrayTravel objectAtIndex:indexPath.row] valueForKey:@"date"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *date = [dateFormatter dateFromString:str];
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *newDateString = [dateFormatter2 stringFromDate:date];
        
        cell.textLabel.text=[NSString stringWithFormat:@"%@", newDateString];
        
        NSString *finalDetailStr=[NSString stringWithFormat:@"%@\n%@",[string stringByConvertingHTMLToPlainText],[self.arrayTravel[indexPath.row][@"comment"] stringByConvertingHTMLToPlainText]];
        cell.detailTextLabel.numberOfLines=0;
        cell.detailTextLabel.text=finalDetailStr ;
        
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        
        labelData.hidden = YES;
        
        UIButton *button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame=CGRectMake(0,0,55,22);
        [button setTitle:[Language get:@"Edit" alter:@"!Edit"] forState:UIControlStateNormal];
        button.layer.cornerRadius = 3;
        button.layer.borderColor = [[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] CGColor];
        button.layer.borderWidth = .8f;
        [button addTarget:self action:@selector(action_EditTravelReport:) forControlEvents:UIControlEventTouchUpInside];
        button.titleLabel.font=[UIFont systemFontOfSize:10];
        // [cell.contentView addSubview:button];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryView=button;
       
        
        
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    headerView.backgroundColor = [UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1.0f];
    [self.view addSubview:headerView];
    
    if (!isSetting) {
        
        UILabel *project = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 320, 20)];
        UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 320, 20)];
        address.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
        
        NSString *stringAdress = [NSString stringWithFormat:@"%@ %@ %@ %@",[self.stringLocation stringByConvertingHTMLToPlainText], [self.stringCity stringByConvertingHTMLToPlainText], [self.stringState stringByConvertingHTMLToPlainText], [self.stringCountry stringByConvertingHTMLToPlainText]];
        
        address.text = stringAdress;
        address.textAlignment = NSTextAlignmentCenter;
        address.textColor=[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
        if(self.stringProjectExtId.length>0)
        {
            project.text=[NSString stringWithFormat:@"[%@] %@", self.stringProjectExtId, [self.stringProjectName stringByConvertingHTMLToPlainText]];
            
        }
        else
        {
            project.text=[NSString stringWithFormat:@"[%@] %@", self.stringProjectId, [self.stringProjectName stringByConvertingHTMLToPlainText]];
            
        }
        project.textAlignment = NSTextAlignmentCenter;
        project.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
        [headerView addSubview:project];
        [headerView addSubview:address];
        
    }
    else
    {
        
        NSDate *monthDisplay = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"MMMM"];
        NSString *stringMonth = [dateFormat stringFromDate:monthDisplay];
        NSLog(@"stringMonth : %@",stringMonth);
        [dateFormat setDateFormat:@"dd/MMMM/yyy"];
        NSString *string = [dateFormat stringFromDate:monthDisplay];
        NSArray *array1 = [string componentsSeparatedByString: @"/"];
        NSString *j = [array1 objectAtIndex: 1];
        
        selectMonth = [UIButton buttonWithType:UIButtonTypeCustom];
        selectMonth.frame = CGRectMake(0, 0, 320, 30);
        if ([[[monthArray objectAtIndex:(NSInteger)row1] valueForKey:@"month"] isEqualToString:stringMonth]) {
            
            // Current Month
            
            [selectMonth setTitle:[NSString stringWithFormat:@"%@ ,Total Hours:%@",j,totalWorkingHours] forState:UIControlStateNormal];
        }
        else
        {
            // Other Month
            [selectMonth setTitle:[NSString stringWithFormat:@"%@ , Total Hours:%@",[[monthArray objectAtIndex:(NSInteger)row1] valueForKey:@"month"],totalWorkingHours] forState:UIControlStateNormal];
            
        }
//        [selectMonth setTitle:(row1==0)?j:[monthArray objectAtIndex:(NSInteger)row1] forState:UIControlStateNormal];
        selectMonth.titleLabel.font = [UIFont systemFontOfSize:15];
        selectMonth.backgroundColor = [UIColor clearColor];
        [selectMonth setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [selectMonth addTarget:self action:@selector(pickerShow:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:selectMonth];
    }
    return headerView;
}

//

#pragma mark - Picker View Delegate
#pragma mark -

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [monthArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
//    NSString *str= [NSString stringWithFormat:@"%@ %@",[[monthArray objectAtIndex:row] valueForKey:@"month"],[[monthArray objectAtIndex:row] valueForKey:@"year"]];
    
    NSString *str= [NSString stringWithFormat:@"%@",[[monthArray objectAtIndex:row] valueForKey:@"month"]];
    
    return str;
    row1 = (int)row;
    
    
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    monthNumber = [[[monthArray objectAtIndex:row] valueForKey:@"monthNumber"] intValue];
    row1 = (int)row;
}
//

#pragma mark - Show Picker
#pragma mark -

-(void)pickerShow : (UIButton *)sender
{
    
    [monthPicker selectRow:row1 inComponent:0 animated:NO];
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        toolBar.frame = CGRectMake(0, APP_DELEGATE.window.frame.size.height-monthPicker.frame.size.height, 320, toolBar.frame.size.height);
    }
                     completion: ^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:1.0 animations:^{
        
        monthPicker.frame = CGRectMake(0, APP_DELEGATE.window.frame.size.height-monthPicker.frame.size.height, 320, monthPicker.frame.size.height);
    }
                     completion: ^(BOOL finished) {
                     }];
}

#pragma mark - Done Button Clicked
#pragma mark -

-(void)doneButton
{
    
    [UIView animateWithDuration:1.0 animations:^{
        
        toolBar.frame = CGRectMake(0, APP_DELEGATE.window.frame.size.height+monthPicker.frame.size.height, 320, toolBar.frame.size.height);
    }
                     completion: ^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:1.0 animations:^{
        
        monthPicker.frame = CGRectMake(0, APP_DELEGATE.window.frame.size.height+monthPicker.frame.size.height, 320, monthPicker.frame.size.height);
    }
                     completion: ^(BOOL finished) {
                     }];
    
    [self postRequestProjectReports];
}


#pragma mark Edit Report

-(void)action_EditTimeReport:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    
    [PersistentStore setEditReport:@"YES"];
    TimeLogVC *timeLogVC=[[TimeLogVC alloc] init];
    timeLogVC.stringProjectExtId = self.stringProjectExtId;
//    timeLogVC.taskName=self.taskName;
    timeLogVC.shouldfromComment = YES;
    timeLogVC.stringProjectId=self.arrayTime[indexPath.row][@"project_id"];
    timeLogVC.stringProjectName=self.arrayTime[indexPath.row][@"title"];
    timeLogVC.e_worker_logid=self.arrayTime[indexPath.row][@"e_worker_logid"];
    timeLogVC.is_from=self.arrayTime[indexPath.row][@"is_from"];
    timeLogVC.is_to=self.arrayTime[indexPath.row][@"is_to"];
    timeLogVC.is_vehicle_used=self.arrayTime[indexPath.row][@"is_vehicle_used"];
    timeLogVC.date=self.arrayTime[indexPath.row][@"date"];
    timeLogVC.tdiff=self.arrayTime[indexPath.row][@"tdiff"];
    timeLogVC.comment=self.arrayTime[indexPath.row][@"comment"];
    
    if (!isSetting) {
    timeLogVC.stringLocation =  self.stringLocation;
    timeLogVC.stringCity =  self.stringCity;
    timeLogVC.stringState =  self.stringState;
    timeLogVC.stringCountry = self.stringCountry;
    timeLogVC.stringHeadId = self.stringHeadId;
    }
    else
    {
    timeLogVC.stringLocation = self.arrayTime[indexPath.row][@"location"];
    timeLogVC.stringCity = self.arrayTime[indexPath.row][@"city"];
    timeLogVC.stringCountry = self.arrayTime[indexPath.row][@"country"];
    timeLogVC.stringState = self.arrayTime[indexPath.row][@"state"];
    timeLogVC.stringHeadId = self.arrayTime[indexPath.row][@"head_id"];
    }
    [self.navigationController pushViewController:timeLogVC animated:YES];
    [PersistentStore setFlagLog:@"NO"];
    [PersistentStore setSkipStatus:@"NO"];
    
    
}

-(void)action_EditMaterialReport:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    
    [PersistentStore setEditReport:@"YES"];
    MaterialExpenseVC *materialExpenseVC=[[MaterialExpenseVC alloc] initWithNibName:nil bundle:nil];
    materialExpenseVC.stringProjectId=self.stringProjectId;
    materialExpenseVC.stringProjectExtId = self.stringProjectExtId;
    materialExpenseVC.stringProjectName=self.stringProjectName;
    materialExpenseVC.stringHeadId = self.stringHeadId;
//    materialExpenseVC.taskName=self.taskName;
    materialExpenseVC.date = self.arrayMaterial[indexPath.row][@"date"];
    materialExpenseVC.comment = self.arrayMaterial[indexPath.row][@"comment"];
    materialExpenseVC.material_amt = self.arrayMaterial[indexPath.row][@"material_amt"];
    materialExpenseVC.material_id = self.arrayMaterial[indexPath.row][@"material_id"];
    materialExpenseVC.material_name = self.arrayMaterial[indexPath.row][@"material_name"];
    materialExpenseVC.order_no = self.arrayMaterial[indexPath.row][@"order_no"];
    materialExpenseVC.order_value = self.arrayMaterial[indexPath.row][@"order_value"];
    materialExpenseVC.project_id = self.arrayMaterial[indexPath.row][@"project_id"];
    materialExpenseVC.stringProjectName = self.arrayMaterial[indexPath.row][@"title"];
    materialExpenseVC.stringProjectId = self.arrayMaterial[indexPath.row][@"project_id"];
    if (!isSetting) {
    materialExpenseVC.stringLocation =  self.stringLocation;
    materialExpenseVC.stringCity =  self.stringCity;
    materialExpenseVC.stringState =  self.stringState;
    materialExpenseVC.stringCountry =  self.stringCountry;
    materialExpenseVC.stringHeadId = self.stringHeadId;

    }
    else
    {
    materialExpenseVC.stringLocation = self.arrayMaterial[indexPath.row][@"location"];
    materialExpenseVC.stringCity = self.arrayMaterial[indexPath.row][@"city"];
    materialExpenseVC.stringCountry = self.arrayMaterial[indexPath.row][@"country"];
    materialExpenseVC.stringState = self.arrayMaterial[indexPath.row][@"state"];
    materialExpenseVC.stringHeadId = self.arrayMaterial[indexPath.row][@"head_id"];
    }
    [self.navigationController pushViewController:materialExpenseVC animated:YES];
    [PersistentStore setFlagLog:@"NO"];
    [PersistentStore setSkipStatus:@"NO"];
}

-(void)action_EditTravelReport:(UIButton *)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    
    [PersistentStore setEditReport:@"YES"];
    TravelExpenseVC *travelExpenseVC=[[TravelExpenseVC alloc] initWithNibName:nil bundle:nil];
    travelExpenseVC.stringProjectId=self.stringProjectId;
    travelExpenseVC.stringProjectExtId = self.stringProjectExtId;
    travelExpenseVC.stringProjectName=self.stringProjectName;
//    travelExpenseVC.taskName = self.taskName;
    travelExpenseVC.stringProjectName = self.arrayTravel[indexPath.row][@"title"];
    travelExpenseVC.stringProjectId = self.arrayTravel[indexPath.row][@"project_id"];
    travelExpenseVC.comment = self.arrayTravel[indexPath.row][@"comment"];
    travelExpenseVC.created_at = self.arrayTravel[indexPath.row][@"created_at"];
    travelExpenseVC.date = self.arrayTravel[indexPath.row][@"date"];
    travelExpenseVC.from = self.arrayTravel[indexPath.row][@"from"];
    travelExpenseVC.head_id = self.arrayTravel[indexPath.row][@"head_id"];
    travelExpenseVC.km = self.arrayTravel[indexPath.row][@"km"];
    travelExpenseVC.other_fee = self.arrayTravel[indexPath.row][@"other_fee"];
    travelExpenseVC.time_from = self.arrayTravel[indexPath.row][@"time_from"];
    travelExpenseVC.time_to = self.arrayTravel[indexPath.row][@"time_to"];
    travelExpenseVC.to = self.arrayTravel[indexPath.row][@"to"];
    travelExpenseVC.worker_id = self.arrayTravel[indexPath.row][@"worker_id"];
    travelExpenseVC.travel_id = self.arrayTravel[indexPath.row][@"travel_id"];
    travelExpenseVC.zone = self.arrayTravel[indexPath.row][@"zone"];
    if (!isSetting) {
    travelExpenseVC.stringLocation =  self.stringLocation;
    travelExpenseVC.stringCity =  self.stringCity;
    travelExpenseVC.stringState =  self.stringState;
    travelExpenseVC.stringCountry =  self.stringCountry;
    travelExpenseVC.stringHeadId = self.stringHeadId;
    }
    else
    {
    travelExpenseVC.stringLocation = self.arrayTravel[indexPath.row][@"location"];
    travelExpenseVC.stringCity = self.arrayTravel[indexPath.row][@"city"];
    travelExpenseVC.stringCountry = self.arrayTravel[indexPath.row][@"country"];
    travelExpenseVC.stringState = self.arrayTravel[indexPath.row][@"state"];
    travelExpenseVC.stringHeadId = self.arrayTravel[indexPath.row][@"head_id"];
    }
    [self.navigationController pushViewController:travelExpenseVC animated:YES];
    [PersistentStore setFlagLog:@"NO"];
    [PersistentStore setSkipStatus:@"NO"];
}

@end

