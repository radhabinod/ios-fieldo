//
//  ProjectDetailsTVC.m
//  Fieldo
//
//  Created by Gagan Joshi on 11/23/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "ProjectDetailsTVC.h"
#import "Language.h"
#import "NSString+HTML.h"
#import "MapView.h"
#import "MapRouteViewController.h"
#import "PersistentStore.h"
#import "ProjectsVC.h"
#import <SplunkMint/SplunkMint.h>

static NSString *CellIdentifier=@"Cell";
static NSString *MapCellIdentifier=@"mapCell";

@interface ProjectDetailsTVC ()<UIAlertViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate>

@end

@implementation ProjectDetailsTVC

{
    NSString *currentCity;
}

@synthesize isAssign;

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[self.arrayProject count]-1)
        return 250.0;
    
    if (indexPath.row==[self.arrayProject count]-2|| indexPath.row==[self.arrayProject count]-3) {
        return 88;
    }
    return  50.0;
}



- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        isAssign=NO;
    }
    return self;
}




- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
     NSString *str_className=NSStringFromClass([self class]);
    [[Mint sharedInstance] leaveBreadcrumb:str_className];
       
    NSLog(@"View frame is: %@", NSStringFromCGRect(self.view.bounds));
    [self showLoadingView];
   
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.title=[Language get:@"Project Details" alter:@"!Project Details"]  ;
    self.arrayCellKeys=[[NSMutableArray alloc] init];
    
    [self.arrayCellKeys addObject:[Language get:@"Start Date" alter:@"!Start Date"]];
  //  [self.arrayCellKeys addObject:[Language get:@"Address" alter:@"!Address"]];
    //[self.arrayCellKeys addObject:[Language get:@"Port Code" alter:@"!Port Code"]];
    [self.arrayCellKeys addObject:[Language get:@"Contact Name" alter:@"!Contact Name"]];//
    [self.arrayCellKeys addObject:[Language get:@"Phone" alter:@"!Phone"]];
    
    if (![[PersistentStore getLoginStatus] isEqualToString:@"Worker"]) {
        [self.arrayCellKeys addObject:[Language get:@"Cost" alter:@"!Cost"]];
    }
   // [self.arrayCellKeys addObject:[Language get:@"Project Type" alter:@"!Project Type"]];
    
    [self.arrayCellKeys addObject:[Language get:@"Description" alter:@"!Description"]];
    [self.arrayCellKeys addObject:[Language get:@"Comments" alter:@"!Comments"]];
    [self.arrayCellKeys addObject:[Language get:@"Map" alter:@"!Map"]];
    
    self.tableView.backgroundColor=[UIColor clearColor];
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 30, 30); // custom frame
    [backButton setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goToPrevious) forControlEvents:UIControlEventTouchUpInside];
    
    // set left barButtonItem to backButton
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    UIButton *assignButton = [UIButton buttonWithType:UIButtonTypeCustom];
    assignButton.frame = CGRectMake(0, 0, 60, 30); // custom frame
    [assignButton setTitle:[Language get:@"Assign" alter:@"!Assign"] forState:UIControlStateNormal];
    [assignButton setTitleColor:self.navigationController.navigationBar.tintColor forState:UIControlStateNormal];
    [assignButton addTarget:self action:@selector(assignButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    // set left barButtonItem to backButton
    if (isAssign) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:assignButton];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    locmanager = [[CLLocationManager alloc] init];
    locmanager.delegate = self;
//    locmanager.distanceFilter = kCLDistanceFilterNone; // whenever we move
//    locmanager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locmanager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locmanager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [locmanager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [locmanager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [locmanager startUpdatingLocation];


}
-(void)viewWillAppear:(BOOL)animated
{
    
    locmanager = [[CLLocationManager alloc] init];
    locmanager.delegate = self;
    //    locmanager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    //    locmanager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locmanager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locmanager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [locmanager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [locmanager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [locmanager startUpdatingLocation];
    
}

-(void) assignButtonClicked
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:[Language get:@"Assign Project" alter:@"!Assign Project"] message:[Language get:@"Are you sure you want to assign this project to yourself" alter:@"!Are you sure you want to assign this project to yourself"] delegate:self cancelButtonTitle:[Language get:@"Cancel" alter:@"!Cancel"] otherButtonTitles:[Language get:@"Assign" alter:@"!Assign"], nil];
    [alertView show];
}


-(void)goToPrevious
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    NSLog(@"Memory Warning just came");
    
    // Dispose of any resources that can be recreated.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.arrayProject count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==[self.arrayProject count]-1)
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MapCellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MapCellIdentifier];
        }

        for (UIView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
        
        
        UILabel  *labelKey = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 50, 20)];
        labelKey.textAlignment=NSTextAlignmentLeft;
        labelKey.text=[Language get:@"Map" alter:@"!Map"];
        labelKey.textColor=[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
        labelKey.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:labelKey] ;
        
        
        UIButton *btnRouteMe=[UIButton buttonWithType:UIButtonTypeCustom];
        btnRouteMe.frame=CGRectMake(220, 5, 100, 20);
        [btnRouteMe setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
        btnRouteMe.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnRouteMe setTitle:[Language get:@"Route Me" alter:@"!Route Me"] forState:UIControlStateNormal];
        [btnRouteMe addTarget:self action:@selector(RouteMe:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnRouteMe];

        
        MKMapView   *mapView=[[MKMapView alloc]initWithFrame:CGRectMake(60,25, 200, 200)];
        mapView.layer.borderColor=[[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] CGColor];
        mapView.layer.borderWidth=1.0;
        mapView.delegate=self;
        [cell.contentView addSubview:mapView];
        
        
        CLLocationCoordinate2D location;
        location.latitude=[self.dictProject[@"project_lat"] doubleValue];
        location.longitude=[self.dictProject[@"project_long"] doubleValue];
        NSLog(@"lat %f long%f",location.latitude,location.longitude);
        
 

        
        MKCoordinateSpan spanObj;
        spanObj.latitudeDelta=0.2;
        spanObj.longitudeDelta=0.2;
        
        MKCoordinateRegion region;
        region.center=location;
        region.span=spanObj;
        
        [mapView setZoomEnabled:YES];
        [mapView setScrollEnabled:YES];
        [mapView setRegion:region animated:YES];
        [mapView regionThatFits:region];
        
        Annotation *annotation=[[Annotation alloc] init];
        annotation.title=self.dictProject[@"location"];
        annotation.coordinate=region.center;
        [mapView addAnnotation:annotation];
        
        
        NSLog(@"%f %f", SearchedLocation.coordinate.latitude, SearchedLocation.coordinate.longitude);
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:SearchedLocation
                       completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 NSLog(@"Geocode failed with error: %@", error);
                 return;
             }
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
             NSLog(@"locality %@",placemark.locality);
             NSLog(@"postalCode %@",placemark.postalCode);
             currentCity =placemark.locality;
             NSLog(@"current city *****:%@",currentCity);
         }];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.textLabel.text=self.arrayCellKeys[indexPath.row];
        cell.textLabel.textColor=[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
        
        cell.detailTextLabel.numberOfLines=0;
        cell.detailTextLabel.text=[self.arrayProject[indexPath.row] stringByConvertingHTMLToPlainText];
       
        cell.detailTextLabel.font=[UIFont systemFontOfSize:15];
        
        if (indexPath.row==2)
        {
          cell.detailTextLabel.textColor = [UIColor blueColor];
            
//          UITapGestureRecognizer *callTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(call:)];
//          callTapGesture.delegate=self;
//          [cell addGestureRecognizer:callTapGesture];

            
            UIButton *btnAccessory=[UIButton buttonWithType:UIButtonTypeCustom];
            btnAccessory.frame=CGRectMake(0, 0, 30, 30);
            [btnAccessory setBackgroundImage:[UIImage imageNamed:@"Call.png"] forState:UIControlStateNormal];
            [btnAccessory addTarget:self action:@selector(Call) forControlEvents:UIControlEventTouchUpInside];
            cell.accessoryView=btnAccessory;
        }
        
        return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==2) {
 
   // [tableView cellForRowAtIndexPath:indexPath].detailTextLabel.textColor = [UIColor redColor];
        NSString *strPhone=[NSString stringWithFormat:@"telprompt://%@",self.dictProject[@"phone"]];
        UIDevice *device=[UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"]) {
            NSURL *url=[NSURL URLWithString:strPhone];
            if ([strPhone isEqualToString:@"telprompt://"]) {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Phone number does not exist." alter:@"!Phone number does not exist."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else
                [[UIApplication sharedApplication] openURL:url];
        }
        else{
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Your device doesn't support this feature." alter:@"!Your device doesn't support this feature."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }

    }

}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row==2) {
//        
//    [tableView cellForRowAtIndexPath:indexPath].detailTextLabel.textColor = [UIColor blueColor];
//    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    [self.view addSubview:headerView];
    
    UILabel *project = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 320, 20)];
    UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 320, 20)];
    address.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    address.text = self.stringAddress;
    address.textColor = [UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
    address.textAlignment = NSTextAlignmentCenter;
    if(self.stringProjectExtId.length>0)
    {
        project.text=[NSString stringWithFormat:@"[%@] %@", self.stringProjectExtId, [self.stringProjectName stringByConvertingHTMLToPlainText]];
        
    }
    else
    {
        project.text=[NSString stringWithFormat:@"[%@] %@", self.stringProjectId, [self.stringProjectName stringByConvertingHTMLToPlainText]];
        
    }
    project.textAlignment = NSTextAlignmentCenter;
    headerView.backgroundColor = [UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1.0f];
    project.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    [headerView addSubview:project];
    [headerView addSubview:address];
    
    return headerView;
}


-(void)RouteMe:(id)sender
{
    
    
//    if (SearchedLocation!=nil) {
//    
//    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
//    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
//        
//        NSString *direction=[NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=%f,%f&daddr=%f,%f&x-success=sourceapp://?resume=true&x-source=AirApp",[self.dictProject[@"project_lat"] doubleValue], [self.dictProject[@"project_long"] doubleValue], SearchedLocation.coordinate.latitude,SearchedLocation.coordinate.longitude];
//        NSURL *directionsURL = [NSURL URLWithString:direction];
//        [[UIApplication sharedApplication] openURL:directionsURL];
//    }
//    else {
//        
//        NSLog(@"SearchedLocation.coordinate.latitude : %f",SearchedLocation.coordinate.latitude);
//        NSLog(@"SearchedLocation.coordinate.longitude : %f",SearchedLocation.coordinate.longitude);
//        
//        NSString *direction=nil;
//        
//        
//       // direction=[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%@,%@&daddr=%@,%@",[NSString stringWithFormat:@"%0.4f",SearchedLocation.coordinate.latitude],[NSString stringWithFormat:@"%0.4f",SearchedLocation.coordinate.longitude],[NSString stringWithFormat:@"%0.4f",[self.dictProject[@"project_lat"] floatValue]],[NSString stringWithFormat:@"%0.4f", [self.dictProject[@"project_long"] floatValue]]];
//        
//                direction=[NSString stringWithFormat:@"https://maps.google.com/?daddr=%@,%@",[NSString stringWithFormat:@"%0.4f",SearchedLocation.coordinate.latitude],[NSString stringWithFormat:@"%0.4f",SearchedLocation.coordinate.longitude]];
//        
//        //        NSURL *directionsURL = [NSURL URLWithString:direction];
//        
//        NSLog(@"direction : %@",direction);
//        NSURL* directionsURL = [[NSURL alloc] initWithString:[direction stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//        [[UIApplication sharedApplication] openURL:directionsURL];
//        //        showAlert(AlertTitle, @"You don't have GoogleMaps App on this device. Please install it.");
//        //NSLog(@"Can't use comgooglemaps-x-callback:// on this device.");
//    }
//        
//    }
//    else
//    {
//        if(IS_OS_8_OR_LATER){
//            NSUInteger code = [CLLocationManager authorizationStatus];
//            if (code == kCLAuthorizationStatusNotDetermined && ([locmanager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locmanager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
//                // choose one request according to your business.
//                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
//                    [locmanager requestAlwaysAuthorization];
//                } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
//                    [locmanager  requestWhenInUseAuthorization];
//                } else {
//                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
//                }
//            }
//        }
//        [locmanager startUpdatingLocation];
//    }



    

    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
        
//        NSString *direction=[NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=%@,%@&daddr=%@,%@&x-success=sourceapp://?resume=true&x-source=AirApp", @"18.9750", @"72.8258", @"23.0300",@"72.5800"];
        
       NSString *direction=[NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=%0.4f,%0.4f&daddr=%0.4f,%0.4f&x-success=sourceapp://?resume=true&x-source=AirApp",SearchedLocation.coordinate.latitude,SearchedLocation.coordinate.longitude, [self.dictProject[@"project_lat"] floatValue],[self.dictProject[@"project_long"] floatValue]];

        NSURL *directionsURL = [NSURL URLWithString:direction];
        [[UIApplication sharedApplication] openURL:directionsURL];
    }
    else {
        
        //NSString *direction=[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%@,%@&daddr=%@,%@", @"18.9750", @"72.8258", @"23.0300",@"72.5800"];

       // NSString *direction=[NSString stringWithFormat:@"http://maps.google.com/maps/place/%@/@%f,%f",locations,location.latitude,location.longitude];
        
       //    NSString *direction=[NSString stringWithFormat:@"http://maps.google.com/maps?q=loc:%f,%f",location.latitude,location.longitude];
      
        CLLocationCoordinate2D location;
        location.latitude=[self.dictProject[@"project_lat"] doubleValue];
        location.longitude=[self.dictProject[@"project_long"] doubleValue];
        NSString *locationss = self.dictProject[@"location"];
        NSString *locations = [locationss stringByConvertingHTMLToPlainText];
        NSLog(@"client location is :%@",locations);
      
//      NSString *direction=[NSString stringWithFormat:@"https://www.google.co.in/maps/dir/%@/%@",currentCity,locations];
        
         NSString *direction=[NSString stringWithFormat:@"https://www.google.co.in/maps/dir/%0.4f,%0.4f/%0.4f,%0.4f",SearchedLocation.coordinate.latitude,SearchedLocation.coordinate.longitude, [self.dictProject[@"project_lat"] floatValue],[self.dictProject[@"project_long"] floatValue]];
          NSLog(@"directions %@",direction);
        
        NSURL* directionsURL = [[NSURL alloc] initWithString:[direction stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL:directionsURL];
        
        
//        showAlert(AlertTitle, @"You don't have GoogleMaps App on this device. Please install it.");
        //NSLog(@"Can't use comgooglemaps-x-callback:// on this device.");
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
//////    MapRouteViewController *routeMap=[[MapRouteViewController alloc]init];
//////    routeMap.dictProject=self.dictProject;
//////    [self.navigationController pushViewController:routeMap animated:YES];
    
    
   // NSLog(@"latutude*******%@",[NSString stringWithFormat:@"%0.4f",SearchedLocation.coordinate.latitude]);
   // NSLog(@"longitude******%@",[NSString stringWithFormat:@"%0.4f",SearchedLocation.coordinate.longitude]);
   // NSLog(@"Point lat ****** %@",[NSString stringWithFormat:@"%0.4f",[self.dictProject[@"project_lat"] floatValue]]);
   // NSLog(@"Point long****** %@",[NSString stringWithFormat:@"%0.4f", [self.dictProject[@"project_long"] floatValue]]);

    
    
}

- (CLLocationCoordinate2D)geoCodeUsingAddress :(NSString *)address Status:(NSInteger )sts
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}



//- (void) call: (UITapGestureRecognizer *)gesture
//{
//    NSString *strPhone=[NSString stringWithFormat:@"telprompt://%@",self.dictProject[@"phone"]];
//    UIDevice *device=[UIDevice currentDevice];
//    if ([[device model] isEqualToString:@"iPhone"]) {
//        NSURL *url=[NSURL URLWithString:strPhone];
//        if ([strPhone isEqualToString:@"telprompt://"]) {
//            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Phone number does not exist." alter:@"!Phone number does not exist."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView show];
//        }
//        else
//            [[UIApplication sharedApplication] openURL:url];
//    }
//    else{
//        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Your device doesn't support this feature." alter:@"!Your device doesn't support this feature."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//    }
//
//}


-(void)Call
{
    
    NSString *strPhone=[NSString stringWithFormat:@"telprompt://%@",self.dictProject[@"phone"]];
    UIDevice *device=[UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"]) {
        NSURL *url=[NSURL URLWithString:strPhone];
        if ([strPhone isEqualToString:@"telprompt://"]) {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Phone number does not exist." alter:@"!Phone number does not exist."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else
            [[UIApplication sharedApplication] openURL:url];
    }
    else{
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Your device doesn't support this feature." alter:@"!Your device doesn't support this feature."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
}


-(NSArray *)arrayProject
{
    if (!_arrayProject)
    {
       
            NSError *error;
            NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
            [postDict setObject:self.stringProjectId forKey:@"project_id"];
        
           if (APP_DELEGATE.isServerReachable) {
            NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
            
            NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECT_DETAIL]];
        
            
            [urlRequest setTimeoutInterval:180];
            NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
            [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
            [urlRequest setHTTPMethod:@"POST"];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                 NSLog(@"project detail response**** %@",object);
                 if (error)
                 {
                     NSLog(@"Error: %@",[error description]);
                 }
                 if ([object isKindOfClass:[NSDictionary class]] == YES)
                 {
                     if ([object[@"CODE"] intValue]==1)
                     {
                         
                     }
                     else
                     {
                         self.dictProject=object[@"data"][0];
                         NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                         [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//                         [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//                         [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                         NSDate *date=[dateFormatter dateFromString:self.dictProject[@"start_date_by_worker"]];
                         NSDateFormatter *dateFormatter2=[[NSDateFormatter alloc] init];
                         [dateFormatter2 setDateFormat:@"dd-MM-YYYY"];
                         
                         NSMutableArray *arrayDetail=[NSMutableArray new];
                         
                         if (date == nil)
                         {
                             NSLog(@"No date");
                             
//                             [arrayDetail addObject:@"0000-00-00"];
                             [arrayDetail addObject:[NSString stringWithFormat:@"This project is in proposal."]];
                         }
                         else
                         {
                             [arrayDetail addObject:[dateFormatter2 stringFromDate:date]];
                         }

//                         [arrayDetail addObject:[self.dictProject[@"start_date_by_worker"] stringByConvertingHTMLToPlainText]];
                         [arrayDetail addObject:[self.dictProject[@"cust_name"] stringByConvertingHTMLToPlainText]];

                         //[arrayDetail addObject:[self.dictProject[@"location"] stringByConvertingHTMLToPlainText]];
                        // [arrayDetail addObject:[self.dictProject[@"zip"] stringByConvertingHTMLToPlainText]];
                         [arrayDetail addObject:[self.dictProject[@"phone"] stringByConvertingHTMLToPlainText]];
                         
                         
                         if (![[PersistentStore getLoginStatus] isEqualToString:@"Worker"]) {
                             [arrayDetail addObject:[self.dictProject[@"cost"] stringByConvertingHTMLToPlainText]];

                         }
                       //  NSString *projectType=[self ProjectType:[self.dictProject[@"project_type"]intValue]];
                        // [arrayDetail addObject:projectType];
                         
                         [arrayDetail addObject:[self.dictProject[@"description"] stringByConvertingHTMLToPlainText]];
                         [arrayDetail addObject:[self.dictProject valueForKey:@"comment"]];
                         [arrayDetail addObject:@" "];

                         
//                         NSArray *arrayDetails=[[NSArray alloc] initWithObjects:self.dictProject[@"created_at"],self.dictProject[@"location"],self.dictProject[@"zip"],
//                                                self.dictProject[@"phone"],self.dictProject[@"projectType"],@" ", nil];
//                         
                         
                         self.arrayProject=arrayDetail;
                         [self performSelectorOnMainThread:@selector(refreshTable) withObject:nil waitUntilDone:YES];
                         
                         
                     }
                 }
             }];
           }
           else
           {
               [self hideLoadingView];
               [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
           }
            
    }
    return _arrayProject;
    
}

-(NSString *) ProjectType:(NSInteger) value
{
    if (value==0) {
        return [Language get:@"Fixed" alter:@"!Fixed"];
    }
    return [Language get:@"Time" alter:@"!Time"];
}

 




-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading...";
    hud.dimBackground = YES;
}

-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)refreshTable
{
  
  self.tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    
    [self.tableView reloadData];
    [self hideLoadingView];
}




-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        [self .navigationController popViewControllerAnimated:YES];
    }
    else{
        if (buttonIndex==1) {
            [self postRequestWorkersProjects];
        }
    }
}

-(void)postRequestWorkersProjects
{
    [self showLoadingView];
    
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    
    
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
    {
        [postDict setObject:[PersistentStore getWorkerID] forKey:@"worker_id"];
         [postDict setObject:self.stringProjectId forKey:@"project_id"];
        
    }
    //    else
    //    {
    //        [postDict setObject:[PersistentStore getCustomerID] forKey:@"cust_id"];
    //    }
    
    
    
    if (APP_DELEGATE.isServerReachable) {
        
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECTS_LIST]];
        
        if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
        {
            //urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECTS_LIST]];
            urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://fieldo.se/api/acceptunassignedproject.php"]];//]@"http://fieldo.se/api/workerprojectlist.php"]];
            
        }
        //    else
        //    {
        //        urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECTS_LIST_CUSTOMER]];
        //
        //    }
        //
        
        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             NSLog(@"%@",object);
             if (error)
             {
                 NSLog(@"Error: %@",[error description]);
             }
             if ([object isKindOfClass:[NSDictionary class]] == YES)
             {
                 if ([object[@"CODE"] intValue]==1)
                 {
                     
                     [self performSelectorOnMainThread:@selector(showAlertNewProjects) withObject:nil waitUntilDone:NO];
                     
                 }
                 else
                 {
                      if ([object[@"MSG"] isEqualToString:@"Success"])
                      {
                          [self performSelectorOnMainThread:@selector(hideLoaderView) withObject:nil waitUntilDone:NO];
                         
                      }
                 }
             }
         }];
    }
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}

-(void)hideLoaderView
{
    [self hideLoadingView];
    
    [self .navigationController popViewControllerAnimated:YES];
}

- (void) showAlertNewProjects
{
    [self hideLoadingView];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fieldo" message:[Language get:@"Failed to Assign.Try Later" alter:@"!Failed to Assign.Try Later"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    
//    alert.tag = 1;
//    
//    [alert show];
}

#pragma mark - CLLocationManagerDelegate
#pragma mark -


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Fieldo" message:[Language get:@"Failed to get your location." alter:@"!Failed to get your location."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    [locmanager stopUpdatingLocation];
    if (currentLocation != nil) {
        SearchedLocation=currentLocation;
//        [self DesignInterface];
    }
    NSLog(@"SearchedLocation : %@",SearchedLocation);
    
}


@end
