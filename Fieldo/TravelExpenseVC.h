//
//  TravelExpenseVC.h
//  Fieldo
//
//  Created by Gagan Joshi on 11/12/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapView.h"
#import <MapKit/MapKit.h>
#import "ConnectionManager.h"


@interface TravelExpenseVC : UITableViewController<UITextFieldDelegate,MapInfoDelegate,ConnectionManager_Delegate>
{
 //   CLLocationManager *locationManager;
    ConnectionManager *Connect;
    
    NSMutableArray *dataArray;
 //   CLLocation *SearchedLocation;

    CLLocationCoordinate2D sourceLoc;
    CLLocationCoordinate2D destLoc;
    
}


@property (nonatomic ,strong) NSMutableArray *referenceNameArray;

@property(nonatomic,retain) NSString *stringProjectId;
@property(nonatomic,retain)  NSString *stringProjectExtId;
@property(nonatomic,retain) NSString *stringProjectName;
@property(nonatomic,retain) NSString *taskName;
@property(nonatomic,retain)  NSString *stringLocation;
@property(nonatomic,retain)  NSString *stringCity;
@property(nonatomic,retain)  NSString *stringState;
@property(nonatomic,retain)  NSString *stringCountry;
@property(nonatomic,retain) NSString *stringHeadId;
@property(nonatomic,retain) NSString *stringFromLocation;
@property(nonatomic,retain) NSString *stringTOLocation;
@property(nonatomic,assign) CLLocationCoordinate2D startLocation;
@property(nonatomic,assign) CLLocationCoordinate2D endLocation;
@property(nonatomic,retain) NSString *stringDistance;

//Mandeep
// Edit log

@property(nonatomic,retain) NSString *comment;
@property(nonatomic,retain) NSString *created_at;
@property(nonatomic,retain) NSString *date;
@property(nonatomic,retain) NSString *from;
@property(nonatomic,retain) NSString *head_id;
@property(nonatomic,retain) NSString *km;
@property(nonatomic,retain) NSString *other_fee;
@property(nonatomic,retain) NSString *time_from;
@property(nonatomic,retain) NSString *time_to;
@property(nonatomic,retain) NSString *to;
@property(nonatomic,retain) NSString *travel_id;
@property(nonatomic,retain) NSString *worker_id;
@property(nonatomic,retain) NSString *zone;

@end
