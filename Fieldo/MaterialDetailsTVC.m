//
//  MaterialDetailsTVC.m
//  Fieldo
//
//  Created by Gagan Joshi on 11/28/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "MaterialDetailsTVC.h"
#import "MBProgressHUD.h"
#import "PersistentStore.h"
#import "Language.h"
#import "NSString+HTML.h"
#import <SplunkMint/SplunkMint.h>
@interface MaterialDetailsTVC ()

@end

@implementation MaterialDetailsTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     NSString *str_className=NSStringFromClass([self class]);
    [[Mint sharedInstance] leaveBreadcrumb:str_className];
    
    self.title=[Language get:@"Material Details" alter:@"!Material Details"];
    self.tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]];

    self.tableView.rowHeight=50.0;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.arrayMaterial[section][@"task"] count])
        return 50;
    return 0.0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([self.arrayMaterial[section][@"external_project_id"] length]>0) {
        
        NSString *string = [NSString stringWithFormat:@"[%@] %@", [self.arrayMaterial[section][@"external_project_id"] stringByConvertingHTMLToPlainText], [self.arrayMaterial[section][@"head_name"] stringByConvertingHTMLToPlainText]];
        
        return string;
    }
    else
    {
    NSString *string = [NSString stringWithFormat:@"[%@] %@", [self.arrayMaterial[section][@"project_id"] stringByConvertingHTMLToPlainText], [self.arrayMaterial[section][@"head_name"] stringByConvertingHTMLToPlainText]];
    
    return string;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    headerView.backgroundColor = [UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1.0f];
    [self.view addSubview:headerView];
    
    UILabel *project = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 320, 20)];
    UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 320, 20)];
    address.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    
    address.text = [self.arrayMaterial[section][@"location"] stringByConvertingHTMLToPlainText];
    address.textColor = [UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
    address.textAlignment = NSTextAlignmentCenter;
    
    if([self.arrayMaterial[section][@"external_project_id"] length]>0)
    {
        project.text=[NSString stringWithFormat:@"[%@] %@", [self.arrayMaterial[section][@"external_project_id"] stringByConvertingHTMLToPlainText], [self.arrayMaterial[section][@"head_name"] stringByConvertingHTMLToPlainText]];
        
    }
    else
    {
        project.text=[NSString stringWithFormat:@"[%@] %@", [self.arrayMaterial[section][@"project_id"] stringByConvertingHTMLToPlainText], [self.arrayMaterial[section][@"head_name"] stringByConvertingHTMLToPlainText]];
        
    }
    
    project.textAlignment = NSTextAlignmentCenter;
    project.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    [headerView addSubview:project];
    [headerView addSubview:address];
    
    return headerView;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.arrayMaterial count];
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayMaterial[section][@"task"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text=[NSString stringWithFormat:@"Task Name : %@",[self.arrayMaterial[indexPath.section][@"task"][indexPath.row][@"task_name"]  stringByConvertingHTMLToPlainText]];
    cell.detailTextLabel.text= [NSString stringWithFormat:@"Material Name : %@",[self.arrayMaterial[indexPath.section][@"task"][indexPath.row][@"material_name"]  stringByConvertingHTMLToPlainText]];

    cell.textLabel.textColor=[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
    cell.textLabel.font=[UIFont systemFontOfSize:12];
    cell.detailTextLabel.font=[UIFont systemFontOfSize:15];
    
    
    return cell;
}


@end
