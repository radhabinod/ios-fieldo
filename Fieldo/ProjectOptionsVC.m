//
//  ProjectOptionsVC.m
//  Fieldo
//
//  Created by Gagan Joshi on 10/26/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "ProjectOptionsVC.h"
#import "CustomerDetailsVC.h"
#import "ProjectDetailsTVC.h"
#import "WorkPlanVC.h"
#import "FloorPlanVC.h"
#import "AppDelegate.h"
#import "Language.h"
#import "PersistentStore.h"
#import "NSString+HTML.h"
#import "WorkersTVC.h"
#import "FloorPlanCVC.h"
#import "LogVC.h"
#import <SplunkMint/SplunkMint.h>
@interface ProjectOptionsVC ()
{
    NSString *address;
}


@end

@implementation ProjectOptionsVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    if (APP_DELEGATE.checkLogView == YES) {
        
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSLog(@"%@",appDelegate.mainMenuVC.contentView);
        
        [appDelegate.mainMenuVC.btnProject setBackgroundImage:[UIImage imageNamed:@"SelectedTop"] forState:UIControlStateNormal];
        [appDelegate.mainMenuVC.btnProject setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [appDelegate.mainMenuVC.btnCalOrRating setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
        [appDelegate.mainMenuVC.btnCalOrRating setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
        
        [appDelegate.mainMenuVC.btnLogOrInvoice setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
        [appDelegate.mainMenuVC.btnLogOrInvoice setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
        
        [appDelegate.mainMenuVC.btnHome setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
        appDelegate.mainMenuVC.btnHome.titleLabel.textColor =[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  50.0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     NSString *str_className=NSStringFromClass([self class]);
    [[Mint sharedInstance] leaveBreadcrumb:str_className];
    
//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
//                                                         forBarMetrics:UIBarMetricsDefault];
//
    
//    self.navigationItem.hidesBackButton=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 30, 30); // custom frame
    [backButton setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goToPrevious) forControlEvents:UIControlEventTouchUpInside];
    
    // set left barButtonItem to backButton
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    NSLog(@"View frame is: %@", NSStringFromCGRect(self.view.bounds));
    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]];
    
//    NSString *projectName = self.currentProject.projectName;
    NSString *projectName;
    
    if (self.currentProject.projectExternalId.length>0) {
        
        projectName = [NSString stringWithFormat:@"[%@]  %@", self.currentProject.projectExternalId, [self.currentProject.projectName stringByConvertingHTMLToPlainText]];
    }
    
    else
    {
        projectName = [NSString stringWithFormat:@"[%@]  %@", self.currentProject.projectId, [self.currentProject.projectName stringByConvertingHTMLToPlainText]];
        
    }

    address = [NSString stringWithFormat:@"%@ %@ %@ %@",[self.currentProject.location stringByConvertingHTMLToPlainText], [self.currentProject.city stringByConvertingHTMLToPlainText], [self.currentProject.state stringByConvertingHTMLToPlainText], [self.currentProject.Country stringByConvertingHTMLToPlainText]];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 44)];
    view.backgroundColor = [UIColor clearColor];

    UILabel *projectName1 = [[UILabel alloc] initWithFrame:CGRectMake(-20, 5, view.frame.size.width, 20)];
    projectName1.text = projectName;//[projectName stringByConvertingHTMLToPlainText];
    projectName1.backgroundColor = [UIColor clearColor];
    projectName1.textColor = [UIColor blackColor];
    projectName1.textAlignment = NSTextAlignmentCenter;
    projectName1.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
//    [projectName1 sizeToFit];
    
    [view addSubview:projectName1];
  
    UILabel *address1 = [[UILabel alloc] initWithFrame:CGRectMake(-40, 20, self.view.frame.size.width, 20)];
    address1.text = [address stringByConvertingHTMLToPlainText];
    address1.backgroundColor = [UIColor clearColor];
    address1.textColor = [UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
    address1.textAlignment = NSTextAlignmentCenter;
    address1.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
//    [address1 sizeToFit];
    
    [view addSubview:address1];

    self.navigationItem.titleView = view;
    

    NSLog(@"theeeeeeeeeeeeeeeeeeeee Navframe Height=%@",
          self.navigationItem.titleView);


//    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
//                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
//                                                           shadow, NSShadowAttributeName,
//                                                           [UIFont fontWithName:@"Helvetica Neue" size:15.0], NSFontAttributeName, nil]];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.arrayProjectDetails=[[NSMutableArray alloc] init];
    self.arrayImages=[[NSMutableArray alloc] init];

    
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
    {
        [self.arrayProjectDetails addObject:[Language get:@"Customer Details" alter:@"!Customer Details"]];
        [self.arrayProjectDetails addObject:[Language get:@"Project Details" alter:@"!Project Details"]];
        [self.arrayProjectDetails addObject:[Language get:@"Work Plan" alter:@"!Work Plan"]];
        [self.arrayProjectDetails addObject:[Language get:@"Floor Plan" alter:@"!Floor Plan"]];
        [self.arrayProjectDetails addObject:[Language get:@"Log" alter:@"!Log"]];
    }
    else
    {
        [self.arrayProjectDetails addObject:[Language get:@"Company Details" alter:@"!Company Details"]];
        [self.arrayProjectDetails addObject:[Language get:@"Project Details" alter:@"!Project Details"]];
        [self.arrayProjectDetails addObject:[Language get:@"Work Plan" alter:@"!Work Plan"]];
        [self.arrayProjectDetails addObject:[Language get:@"Floor Plan" alter:@"!Floor Plan"]];
        [self.arrayProjectDetails addObject:[Language get:@"Workers" alter:@"!Workers"]];
    }
    
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
    {
        [self.arrayImages addObject:@"CustomerDetails.png"];
        [self.arrayImages addObject:@"ProjectDetails.png"];
        [self.arrayImages addObject:@"WorkPlan.png"];
        [self.arrayImages addObject:@"FloorPlan.png"];
        [self.arrayImages addObject:@"Log.png"];
    }
    else
    {
        [self.arrayImages addObject:@"CompanyDetails.png"];
        [self.arrayImages addObject:@"ProjectDetails.png"];
        [self.arrayImages addObject:@"WorkPlan.png"];
        [self.arrayImages addObject:@"FloorPlan.png"];
        [self.arrayImages addObject:@"Workers.png"];
    }
    
    
                              
                              
}
-(void)goToPrevious
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayProjectDetails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"std"];
        
        
        // 1: To provide feedback to the user, create a UIActivityIndicatorView and set it as the cellís accessory view.
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imageView.image=[UIImage imageNamed:[self.arrayImages objectAtIndex:indexPath.row]];

    cell.textLabel.text=[self.arrayProjectDetails objectAtIndex:indexPath.row];
    // Configure the cell...
    
    return cell;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
    {
        switch (indexPath.row)
        {
            case 0:
            {
                CustomerDetailsVC *customerDetailsVC=[[CustomerDetailsVC alloc] initWithNibName:nil bundle:nil];
                customerDetailsVC.stringProjectId=self.currentProject.projectId;
                customerDetailsVC.stringProjectExtId=self.currentProject.projectExternalId;
                customerDetailsVC.stringAddress=address;
                customerDetailsVC.stringProjectName = self.currentProject.projectName;
                [self.navigationController pushViewController:customerDetailsVC animated:NO];
                break;
            }
                
            case 1:
            {
                
                ProjectDetailsTVC *projectDetailsTVC=[[ProjectDetailsTVC alloc] initWithNibName:nil bundle:nil];
                projectDetailsTVC.stringProjectId=self.currentProject.projectId;
                projectDetailsTVC.stringProjectExtId=self.currentProject.projectExternalId;
                projectDetailsTVC.stringAddress=address;
                projectDetailsTVC.stringWorkerId=[PersistentStore getWorkerID];
                projectDetailsTVC.stringProjectName = self.currentProject.projectName;
                [self.navigationController pushViewController:projectDetailsTVC animated:YES];
                break;
                
            }
                
            case 2:
            {
                WorkPlanVC *workPlanVC=[[WorkPlanVC alloc] init];
                workPlanVC.stringProjectId = self.currentProject.projectId;
                workPlanVC.stringProjectExtId=self.currentProject.projectExternalId;
                workPlanVC.stringAddress=address;
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:self.currentProject.projectName forKey:@"ProjectName"];
                [defaults synchronize];
                workPlanVC.stringProjectName = self.currentProject.projectName;

                [self.navigationController pushViewController:workPlanVC animated:YES];
                break;
            }
                
            case 3:
            {
                
                FloorPlanCVC *floorPlanCVC=[[FloorPlanCVC alloc] init];
                floorPlanCVC.stringProjectId=self.currentProject.projectId;
                floorPlanCVC.stringProjectExtId=self.currentProject.projectExternalId;
                floorPlanCVC.stringProjectName = self.currentProject.projectName;
                floorPlanCVC.stringAddress=address;

                [self.navigationController pushViewController:floorPlanCVC animated:YES];
                break;
            }
                
            case 4:
            {
                
                APP_DELEGATE.checkLogView = YES;
                
                AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                NSLog(@"%@",appDelegate.mainMenuVC.contentView);
                
                [appDelegate.mainMenuVC.btnProject setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
                [appDelegate.mainMenuVC.btnProject setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
                
                [appDelegate.mainMenuVC.btnCalOrRating setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
                [appDelegate.mainMenuVC.btnCalOrRating setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
                
                [appDelegate.mainMenuVC.btnLogOrInvoice setBackgroundImage:[UIImage imageNamed:@"SelectedTop"] forState:UIControlStateNormal];
                [appDelegate.mainMenuVC.btnLogOrInvoice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [appDelegate.mainMenuVC.btnHome setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
                appDelegate.mainMenuVC.btnHome.titleLabel.textColor =[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
                
                if([appDelegate.mainMenuVC.contentView.subviews count] == 1)
                {
                    [[appDelegate.mainMenuVC.contentView.subviews objectAtIndex:0] removeFromSuperview];
                }
                
                UIViewController* controller = (UIViewController*)[appDelegate.mainMenuVC.childViewControllers objectAtIndex:2];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:self.currentProject.projectName forKey:@"ProjectName"];
                [defaults synchronize];
                controller.view.frame = appDelegate.mainMenuVC.contentView.bounds;
                
                // Prabhjot
                
                if([controller isKindOfClass:[LogVC class]])
                {
                    LogVC *logVC = (LogVC*)controller;
                    logVC.shouldSelectProjectBtn = NO;
                } else if([controller isKindOfClass:[UINavigationController class]]) {
                    if ([[(UINavigationController*)controller topViewController] isKindOfClass:[LogVC class]]) {
                        LogVC *logVC = (LogVC*)[(UINavigationController*)controller topViewController];
                        logVC.shouldSelectProjectBtn = NO;
                    }
                }
                
                //Stop
                
                //Mandeep
                [PersistentStore setFlagLog:@"YES"];
                
                [appDelegate.mainMenuVC.contentView addSubview:controller.view];
                
                break;
            }
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
            {
               
                CustomerDetailsVC *customerDetailsVC=[[CustomerDetailsVC alloc] initWithNibName:nil bundle:nil];
                customerDetailsVC.stringProjectId=self.currentProject.projectId;
                customerDetailsVC.stringProjectExtId=self.currentProject.projectExternalId;
                customerDetailsVC.stringAddress=address;
                customerDetailsVC.stringProjectName = self.currentProject.projectName;
                [self.navigationController pushViewController:customerDetailsVC animated:NO];
                break;
            }
                
            case 1:
            {
                ProjectDetailsTVC *projectDetailsTVC=[[ProjectDetailsTVC alloc] initWithNibName:nil bundle:nil];
                projectDetailsTVC.stringProjectId=self.currentProject.projectId;
                projectDetailsTVC.stringProjectExtId=self.currentProject.projectExternalId;
                projectDetailsTVC.stringAddress=address;
                projectDetailsTVC.stringWorkerId=[PersistentStore getWorkerID];
                projectDetailsTVC.stringProjectName = self.currentProject.projectName;
                [self.navigationController pushViewController:projectDetailsTVC animated:YES];
                break;
            }
                
            case 2:
            {
                WorkPlanVC *workPlanVC=[[WorkPlanVC alloc] initWithNibName:nil bundle:nil];
                workPlanVC.stringProjectId = self.currentProject.projectId;
                workPlanVC.stringProjectExtId=self.currentProject.projectExternalId;
                workPlanVC.stringAddress=address;
                workPlanVC.stringProjectName = self.currentProject.projectName;
                [self.navigationController pushViewController:workPlanVC animated:YES];
                break;
                
            }
                
            case 3:
            {
                FloorPlanCVC *floorPlanCVC=[[FloorPlanCVC alloc] initWithNibName:nil bundle:nil];
                floorPlanCVC.stringProjectId=self.currentProject.projectId;
                floorPlanCVC.stringProjectExtId=self.currentProject.projectExternalId;
                floorPlanCVC.stringAddress=address;
                floorPlanCVC.stringProjectName = self.currentProject.projectName;
                [self.navigationController pushViewController:floorPlanCVC animated:YES];
                break;               
                
            }
            case 4:
            {
                NSLog(@"workers List");
                WorkersTVC *workersTVC=[[WorkersTVC alloc] initWithNibName:nil bundle:nil];
                workersTVC.stringProjectId=self.currentProject.projectId;
                workersTVC.stringProjectExtId=self.currentProject.projectExternalId;
                workersTVC.stringAddress=address;
                workersTVC.navigateFrom=@"Project";
                workersTVC.title=[Language get:@"Worker List" alter:@"!Worker List"];
                workersTVC.stringProjectName = self.currentProject.projectName;
                [self.navigationController pushViewController:workersTVC animated:YES];
               
                break;
                
            }
                
            default:
                break;
        }

    }
  
}

@end
