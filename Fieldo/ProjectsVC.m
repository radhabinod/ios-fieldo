//
//  ProjectVC.m
//  Fieldo
//
//  Created by Gagan Joshi on 10/26/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "ProjectsVC.h"
#import "ProjectOptionsVC.h"
#import "MBProgressHUD.h"
#import "Language.h"
#import "PersistentStore.h"
#import "NSString+HTML.h"
#import "Helper.h"
#import "ProjectOptionsVC.h"
#import "CustomerDetailsVC.h"
#import "ProjectDetailsTVC.h"
#import "WorkPlanVC.h"
#import "FloorPlanVC.h"
#import "AppDelegate.h"
#import "Language.h"
#import "PersistentStore.h"
#import "NSString+HTML.h"
#import "WorkersTVC.h"
#import "FloorPlanCVC.h"
#import <QuartzCore/QuartzCore.h>
#import "NewProjectsListTableViewController.h"
#import "addProjectViewController.h"
#import <SplunkMint/SplunkMint.h>
#import "CurrentLocationView.h"

@interface ProjectsVC ()<UISearchBarDelegate,UISearchDisplayDelegate>

@end

@implementation ProjectsVC
{
    NSMutableArray *searchResults;
    NSString *searchType;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  52.0;
}



#pragma mark NsOperation Queue group

- (PendingOperations *)pendingOperations
{
    if (!_pendingOperations)
    {
        _pendingOperations = [[PendingOperations alloc] init];
    }
    return _pendingOperations;
}



- (void)startOperationsForPhotoRecord:(ProjectRecord *)record atIndexPath:(NSIndexPath *)indexPath {
    
    // 2: You inspect it to see whether it has an image; if so, then ignore it.
    if (!record.hasImage) {
        
        // 3: If it does not have an image, start downloading the image by calling startImageDownloadingForRecord:atIndexPath: (which will be implemented shortly). Youíll do the same for filtering operations: if the image has not yet been filtered, call startImageFiltrationForRecord:atIndexPath: (which will also be implemented shortly).
        [self startImageDownloadingForRecord:record atIndexPath:indexPath];
        
    }
    
    
}


- (void)startImageDownloadingForRecord:(ProjectRecord *)record atIndexPath:(NSIndexPath *)indexPath
{
   
    // 1: First, check for the particular indexPath to see if there is already an operation in downloadsInProgress for it. If so, ignore it.
    if (![self.pendingOperations.downloadsInProgress.allKeys containsObject:indexPath]) {
        
        // 2: If not, create an instance of FlyerDownloader by using the designated initializer, and set ListViewController as the delegate. Pass in the appropriate indexPath and a pointer to the instance of PhotoRecord, and then add it to the download queue. You also add it to downloadsInProgress to help keep track of things.
        // Start downloading
        ProjectDownloader *projectDownloader = [[ProjectDownloader alloc] initWithProjectRecord:record atIndexPath:indexPath delegate:self];
        [self.pendingOperations.downloadsInProgress setObject:projectDownloader forKey:indexPath];
        [self.pendingOperations.downloadQueue addOperation:projectDownloader];
    }
    
}


- (void)projectDownloaderDidFinish:(ProjectDownloader *)downloader
{

        NSIndexPath *indexPath = downloader.indexPathInTableView;
        ProjectRecord *theRecord = downloader.projectRecord;

        [self.arrayProjects replaceObjectAtIndex:indexPath.row withObject:theRecord];
        //[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.pendingOperations.downloadsInProgress removeObjectForKey:indexPath];
         [self hideLoadingView];
    
    //[self performSelectorOnMainThread:@selector(refreshTable) withObject:nil waitUntilDone:NO];
 
    
  
}




#pragma mark showHideView
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading...";
    hud.dimBackground = YES;
}


-(void)refreshTable
{
       [self.tableView reloadData];
    [self hideLoadingView];

    
}


-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
  //  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


-(void)hideLoadingViewed
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
  //  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}


- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
     NSString *str_className=NSStringFromClass([self class]);
    [[Mint sharedInstance] leaveBreadcrumb:str_className];
    
    self.navigationController.navigationBar.translucent=NO;
    searchResults = [[NSMutableArray alloc] init];

    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [searchBar sizeToFit];
    searchBar.delegate = self;
    [self.tableView setTableHeaderView:searchBar];
    

    
    self.tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]];
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
     self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
     //[self.tableView setContentInset:UIEdgeInsetsMake(50, 0, 0, 0)];
    
    [self showLoadingView];
  [self postRequestWorkersProjects];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuth:)
     name:@"ToProjectsVCRefresh"
     object:nil];

    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuth:)
     name:@"ToProjectsVC"
     object:nil];

    
   [self performSelector:@selector(showSearchBar) withObject:nil afterDelay:0.7];
}


- (void) receiveToggleAuth:(NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"ToProjectsVC"]) {
        
            self.navigationController.navigationBar.translucent=NO;
        [self showLoadingView];

        searchResults = [[NSMutableArray alloc] init];
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        [searchBar sizeToFit];
        searchBar.delegate = self;
        [self.tableView setTableHeaderView:searchBar];
        
        self.tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]];
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];

        [self postRequestWorkersProjects];
          //NSLog(@"view did load calls***");
        [self performSelector:@selector(showSearchBar) withObject:nil afterDelay:0.7];
        
        [self isBeingPresented];
    }
    
    else if ([[notification name] isEqualToString:@"ToProjectsVCRefresh"])
    {
        
        
         [self postRequestWorkersProjects];
        
    }
    
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // other stuff

}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}



-(void)viewWillLayoutSubviews
{
    if(self.searchDisplayController.isActive)
    {
        [UIView animateWithDuration:0.001 delay:0.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [self.navigationController setNavigationBarHidden:NO animated:NO];
        }completion:nil];
    }
    [super viewWillLayoutSubviews];
}




- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBars
{
    [searchBars resignFirstResponder];
    NSDictionary *search = @{@"searchText":searchBars.text};
    //NSLog(@"search text away %@",search);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"getSearchingText" object:self userInfo:search];

}




- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBars
{
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if([text length] > 0)
    {

        filteredTableData = [[NSMutableArray alloc] init];

        
        for (ProjectRecord* record in self.arrayProjects)
        {
            NSRange projectNameRange = [record.projectName rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange projectIdRange = [record.projectId rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange locationRange = [record.location rangeOfString:text options:NSCaseInsensitiveSearch];
            
            NSRange cityRange = [record.city rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange stateRange = [record.state rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange countryRange = [record.Country rangeOfString:text options:NSCaseInsensitiveSearch];

            if (projectNameRange.length >0 || projectIdRange.length >0 || locationRange.length >0 || cityRange.length >0 || stateRange.length >0 || countryRange.length >0) {
                [filteredTableData addObject:record];
                //NSLog(@"filtered array count is%lu   %@",(unsigned long)[filteredTableData count], filteredTableData);
            }
        }
     //   NSLog(@"total no of projects is %lu  and projects:%@",(unsigned long)[self.arrayProjects count],self.arrayProjects);

        
        
        if (searchResults) {
          searchResults = nil;
        }
        
        
        searchResults = [NSMutableArray arrayWithArray:filteredTableData];
        
        filteredTableData = nil;
    } else {
        searchResults = [NSMutableArray arrayWithArray:self.arrayProjects];
    }


    
    [self.tableView reloadData];
}



-(void) NewProjects:(id)sender
{
    NewProjectsListTableViewController *newproject=[[NewProjectsListTableViewController alloc]initWithNibName:nil bundle:nil];
    newproject.isAssign=NO;
    [self.navigationController pushViewController:newproject animated:YES];
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    

//     [searchBar becomeFirstResponder];
    
    
//    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
//    searchBar.userInteractionEnabled = YES;
//    searchDisplayController.delegate = self;
//    //searchDisplayController.searchResultsDelegate = self;
//    searchDisplayController.searchResultsDataSource = self;
    

    
    [super viewWillAppear:YES];
    
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;

    

    
    //set the delegate = self. Previously declared in ViewController.h
  
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.mainMenuVC.btnHome setBackgroundImage:[UIImage imageNamed:@"UnSelectedTop"] forState:UIControlStateNormal];
    [appDelegate.mainMenuVC.btnHome setTitleColor:[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f] forState:UIControlStateNormal];
    

    for(UIView* view in self.navigationController.navigationBar.subviews)
    {
        if (view.tag == -101 || view.tag == -200)
        {
            [view removeFromSuperview];
        }
        
    }
    
    
    [self currentLocation];
    self.navigationItem.title=[Language get:@"Projects" alter:@"!Projects"];
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"]) {

        //create the button and assign the image
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"newIconblue.png"] forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, 30, 30);
        [button addTarget:self action:@selector(NewProjects:) forControlEvents:UIControlEventTouchUpInside];
        
        //create a UIBarButtonItem with the button as a custom view
        UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = customBarItem;
        
        
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"project_permission"] integerValue]==1 && [[[NSUserDefaults standardUserDefaults]objectForKey:@"can_assign_themselves"] integerValue]==1)
        {
            UIBarButtonItem* rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewProject:)];
            
            self.navigationItem.rightBarButtonItem = rightButton;
        }
    }
    
    
 

    
//      [self postRequestWorkersProjects];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuthUINotification:)
     name:@"getSearchingText"
     object:nil];
    

    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBars
{
    
    
}


-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBars {
    
      [searchBars resignFirstResponder];

}


- (void) receiveToggleAuthUINotification:(NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"getSearchingText"]) {
        
         NSString *searchText = [notification userInfo][@"searchText"];
       // searchBar.text =@"hiss";
        
     
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:searchText forKey:@"searchKey"];
        [userDefaults synchronize];
       // searchType = @"";
        NSLog(@"Search text saved ******%@",searchText);
        
    }
}


-(void)addNewProject:(id)sender
{
    //Add new project here
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"project_permission"] integerValue]==1 && [[[NSUserDefaults standardUserDefaults]objectForKey:@"can_assign_themselves"] integerValue]==1)
    {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:[ Language get:@"Choose an Option" alter:@"!Choose an Option"] message:nil delegate:self cancelButtonTitle:[Language get:@"Cancel" alter:@"!Cancel"] otherButtonTitles:[Language get:@"Create a new project" alter:@"!Create a new project"],[Language get:@"Assign me a project from the list" alter:@"!Assign me a project from the list"], nil];
        alertView.tag=899;
        [alertView show];

        
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"project_permission"] integerValue]==0 && [[[NSUserDefaults standardUserDefaults]objectForKey:@"can_assign_themselves"] integerValue]==1)
    {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:[ Language get:@"Choose an Option" alter:@"!Choose an Option"] message:nil delegate:self cancelButtonTitle:[Language get:@"Cancel" alter:@"!Cancel"] otherButtonTitles:[Language get:@"Assign me a project from the list" alter:@"!Assign me a project from the list"], nil];
        alertView.tag=899;
        [alertView show];
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"project_permission"] integerValue]==1 && [[[NSUserDefaults standardUserDefaults]objectForKey:@"can_assign_themselves"] integerValue]==0)
    {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:[ Language get:@"Choose an Option" alter:@"!Choose an Option"] message:nil delegate:self cancelButtonTitle:[Language get:@"Cancel" alter:@"!Cancel"] otherButtonTitles:[Language get:@"Create a new project" alter:@"!Create a new project"], nil];
        alertView.tag=899;
        [alertView show];
    }
}


#pragma mark - CLLocation For Current Location
-(void)currentLocation{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    if(IS_OS_8_OR_LATER){
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate
#pragma mark -


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Fieldo" message:[Language get:@"Failed to get your location." alter:@"!Failed to get your location."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        SearchedLocation = currentLocation;
        [locationManager stopUpdatingLocation];
        
        if ([[USER_LOGINID valueForKey:@"userLogin"] isEqualToString:@"0"]) {
            CurrentLocationView *currentView = [[CurrentLocationView alloc] init];
            currentView.longitude = [NSString stringWithFormat:@"%f",SearchedLocation.coordinate.longitude];
            currentView.latitude = [NSString stringWithFormat:@"%f",SearchedLocation.coordinate.latitude];
            [currentView userCurrentLocation:self.view];
        }
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)postRequestWorkersProjects
{
[self showLoadingView];
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    
    
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
    {
        [postDict setObject:[PersistentStore getWorkerID] forKey:@"worker_id"];
    }
    else
    {
        [postDict setObject:[PersistentStore getCustomerID] forKey:@"cust_id"];
        [postDict setObject:[PersistentStore getLoginEmailCustomer] forKey:@"email"];
        //        jsonObject.put("email", pref.getCustomerEmail());
    }
    
    
    
    if (APP_DELEGATE.isServerReachable) {
        
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECTS_LIST]];
        
        if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
        {
            //urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECTS_LIST]];
            urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECTS_LIST]];//]@"http://fieldo.se/api/workerprojectlist.php"]];
            
        }
        else
        {
            urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_PROJECTS_LIST_CUSTOMER]];
            
        }
        
        
        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (data == nil)
             {
                 [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Server is down please try again later." alter:@"!Server is down please try again later."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
             }
             else
             {
                 id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                 NSLog(@"projects listing is :%@",object);
                 if (error)
                 {
                     //NSLog(@"Error: %@",[error description]);
                 }
                 if ([object isKindOfClass:[NSDictionary class]] == YES)
                 {
                     if ([object[@"CODE"] intValue]==1)
                     {
                         [self performSelectorOnMainThread:@selector(showAlert) withObject:nil waitUntilDone:NO];
                     }
                     else
      {

                         
                         NSMutableArray *objEvents=object[@"data"];
                         NSMutableArray *records = [@[] mutableCopy];
                         for(NSMutableDictionary *objEvent in objEvents)
                         {
                             //                     @autoreleasepool
                             //                     {
                             ProjectRecord *project=[[ProjectRecord alloc] init];
                             project.projectId=objEvent[@"project_id"];
                             project.projectExternalId=objEvent[@"external_project_id"];
                             project.projectName=objEvent[@"title"];
                             project.projectType=objEvent[@"project_type"];
                             project.projectDescription=objEvent[@"description"];
                             project.projectUnreadMsg=objEvent[@"unreadmsg"];
                             project.projectImageURL=[NSURL URLWithString:objEvent[@"file_name"]];
                             project.location=(objEvent[@"location"])?objEvent[@"location"]:@"";
                             project.city=objEvent[@"city"]?objEvent[@"city"]:@"";
                             project.state=objEvent[@"state"]?objEvent[@"state"]:@"";
                             project.Country=objEvent[@"country"]?objEvent[@"country"]:@"";
                             project.status=objEvent[@"is_completed"];
                             [records addObject:project];
                             project=nil;
                             
                             //                     }
                             
                         }
                         
                         
                         
                         self.arrayProjects=records;
          
          
                             
          
                         
          if ([searchBar.text isEqualToString:@""]) {
              [searchResults removeAllObjects];
              searchResults = [NSMutableArray arrayWithArray:self.arrayProjects];
              //NSLog(@"Reload all data^^^^^^^^^^^^ ");
          }
                     
    
          //dispatch_async(dispatch_get_main_queue(), ^{
[self performSelectorOnMainThread:@selector(refreshTable) withObject:nil waitUntilDone:NO];
         // });
          
                         
                         
                         for (int i=0;i<[searchResults count];i++)
                         {
                             ProjectRecord *projectRecord=[searchResults objectAtIndex:i];
                             [self startOperationsForPhotoRecord:projectRecord atIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                             
                         }
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSString *searchString = [userDefaults objectForKey:@"searchKey"];
         // NSLog(@"Get Search text------%@",searchString);
                                  //  [searchResults removeAllObjects];
                                     //[filteredTableData removeAllObjects];
          //                             [self.tableView reloadData];
          searchBar.text=searchString;
          [self searchBar:searchBar textDidChange:searchString];
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.tableView reloadData];
          });
          

          
          
          
                     }
                 }
             }
         }];
    }
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
    
    
}


- (void) showSearchBar
{
    

    
  //  [searchBar becomeFirstResponder];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *searchString = [userDefaults objectForKey:@"searchKey"];
    NSLog(@"Get Search text------%@",searchString);
    if (searchString.length>0) {
        [self performSelectorOnMainThread:@selector(hideLoadingAfterView) withObject:nil waitUntilDone:0.3];
        //[self hideLoadingView];

    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    searchBar.text=searchString;
  
}

-(void)hideLoadingAfterView
{
    [self hideLoadingViewed];
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
  //  return [self.arrayProjects count];
    
//     if (tableView == searchDisplayController.searchResultsTableView)
//    return searchResults.count;
    return searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *statusLabel=nil;
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        statusLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-10, 5, 10, cell.frame.size.height-5)];
        statusLabel.tag=1221;
        statusLabel.numberOfLines=1;
        statusLabel.clipsToBounds=YES;
//      statusLabel.layer.cornerRadius=8;
        [cell addSubview:statusLabel];
        
    } else {
        statusLabel = (UILabel *)[cell viewWithTag:1221];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   


    ProjectRecord *aRecord = [searchResults objectAtIndex:indexPath.row];
    NSLog(@"main array values++++++++++ : %@",searchResults);
  
    if ([aRecord.status boolValue]) {
        statusLabel.backgroundColor=[UIColor colorWithRed:73/255.f green:120/255.f blue:38/255.f alpha:1.0];
        statusLabel.text=@"";
    }
    else
    {
        statusLabel.backgroundColor=[UIColor redColor];//colorWithRed:253/255.f green:110/255.f blue:95/255.f alpha:1.0];
        statusLabel.text=@"";
    }

    NSString *projectNameString;
    
    if (aRecord.projectExternalId.length>0){
        
        projectNameString=[NSString stringWithFormat:@"[%@] %@",aRecord.projectExternalId,[aRecord.projectName stringByConvertingHTMLToPlainText]];
    }
    else
    {
        projectNameString=[NSString stringWithFormat:@"[%@] %@",aRecord.projectId,[aRecord.projectName stringByConvertingHTMLToPlainText]];
    }

    
    if (aRecord.hasImage)
    {
        cell.textLabel.text = projectNameString;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
        cell.textLabel.numberOfLines = 2;
        cell.imageView.image=[Helper maskImage:aRecord.projectImage withMask:[UIImage imageNamed:@"maskRect.png"]];
    }
    else if (aRecord.isFailed)
    {
        
        cell.textLabel.text = projectNameString;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
        cell.textLabel.numberOfLines = 2;
        cell.imageView.image=[Helper maskImage:[UIImage imageNamed:@"your-photo.png"] withMask:[UIImage imageNamed:@"maskRect.png"]];
        cell.imageView.image=[Helper maskImage:[UIImage imageNamed:@"NoArtwork.png"] withMask:[UIImage imageNamed:@"maskRect.png"]];

    }
    else
    {
      //  imageCell.image = [UIImage imageNamed:@"Placeholder.png"];
    cell.textLabel.text =  projectNameString;
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    cell.textLabel.numberOfLines = 2;
    cell.imageView.image=aRecord.projectImage;
    cell.imageView.image=[Helper maskImage:[UIImage imageNamed:@"NoArtwork.png"] withMask:[UIImage imageNamed:@"maskRect.png"]];

    }

    cell.detailTextLabel.text=[NSString stringWithFormat:@"%@ %@ %@ %@",[aRecord.location stringByConvertingHTMLToPlainText],[aRecord.city stringByConvertingHTMLToPlainText],[aRecord.state stringByConvertingHTMLToPlainText],[aRecord.Country stringByConvertingHTMLToPlainText]];//[aRecord.projectDescription stringByConvertingHTMLToPlainText];
  
    cell.detailTextLabel.textColor=[UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0f];
  
    
    if ([aRecord.projectUnreadMsg isEqualToString:@"0"] )
    {
       UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 16)];
       imageView.image=[UIImage imageNamed:@"imageUnreadMsg@2x.png"];
       cell.accessoryView=imageView;
    }
    else
    {
        cell.accessoryView=nil;
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    
 
    return cell;
 
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (searchResults.count==0)
    {
       
    }
    else
    {
    
    ProjectRecord *aRecord = [searchResults objectAtIndex:indexPath.row];
    flag = (int)indexPath.row;
    ProjectOptionsVC *projectOptionsVC=[[ProjectOptionsVC alloc] init];
    projectOptionsVC.currentProject=aRecord;
        NSLog(@"current project get %@",aRecord);
    [self.navigationController pushViewController:projectOptionsVC animated:YES];
        
    }
}

-(void)postRequestWorkersPosition
{
    
   
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    
    
    if ([[PersistentStore getLoginStatus] isEqualToString:@"Worker"])
    {
        [postDict setObject:[PersistentStore getWorkerID] forKey:@"worker_id"];
    }
    
    [postDict setObject:[NSString stringWithFormat:@"%f",SearchedLocation.coordinate.longitude] forKey:@"long"];
    [postDict setObject:[NSString stringWithFormat:@"%f",SearchedLocation.coordinate.latitude] forKey:@"lat"];

    if (APP_DELEGATE.isServerReachable) {
        
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://fieldo.se/api/setLat_long.php"]];
        

        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             //NSLog(@"%@",object);
             if (error)
             {
                 NSLog(@"Error: %@",[error description]);
             }
             if ([object isKindOfClass:[NSDictionary class]] == YES)
             {
                 if ([object[@"CODE"] intValue]==1)
                 {
//                     [self performSelectorOnMainThread:@selector(refreshTable) withObject:nil waitUntilDone:NO];
                 }
                 else
                 {
//                     NSMutableArray *objEvents=object[@"data"];
                     
                 }
             }
         }];
    }
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}




#pragma mark - UIAlertView Delegate

- (void) showAlert
{
    [self hideLoadingView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fieldo" message:[Language get:@"No data found." alter:@"!No data found."] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    alert.tag = 1;
    
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonStr=[alertView buttonTitleAtIndex:buttonIndex];
    if (alertView.tag==899) {
        if ([buttonStr isEqualToString:[Language get:@"Create a new project" alter:@"!Create a new project"]]) {
            addProjectViewController *project=[[addProjectViewController alloc]initWithNibName:@"addProjectViewController" bundle:nil];
            [self.navigationController pushViewController:project animated:YES];
        }
        else if ([buttonStr isEqualToString:[Language get:@"Assign me a project from the list" alter:@"!Assign me a project from the list"]]) {
            NewProjectsListTableViewController *newproject=[[NewProjectsListTableViewController alloc]initWithNibName:nil bundle:nil];
            newproject.isAssign=YES;
            [self.navigationController pushViewController:newproject animated:YES];
        }
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"customerID"];
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"customerNAME"];
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"selectedWorkers"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self cancelAllOperations];
    [self.view endEditing:YES];
}





#pragma mark - Cancelling, suspending, resuming queues / operations
- (void)suspendAllOperations
{
    [self.pendingOperations.downloadQueue setSuspended:YES];
    
}

- (void)resumeAllOperations {
    [self.pendingOperations.downloadQueue setSuspended:NO];
    
}

- (void)cancelAllOperations {
    [self.pendingOperations.downloadQueue cancelAllOperations];
    
}





@end
