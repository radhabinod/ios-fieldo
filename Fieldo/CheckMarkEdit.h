//
//  CheckMark.h
//  Fieldo
//
//  Created by Parveen Sharma on 8/12/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckMarkEdit : UITableViewCell
@property (nonatomic,strong) IBOutlet UIButton *checkBtn;
@property (nonatomic,strong) IBOutlet UILabel  *vehicle;
@property (nonatomic,strong) IBOutlet UILabel  *lunch;
@property (weak, nonatomic) IBOutlet UIButton *button_editLunch;


@end
