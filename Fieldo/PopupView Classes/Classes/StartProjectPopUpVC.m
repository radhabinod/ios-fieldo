//
//  PopUpVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 15/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "StartProjectPopUpVC.h"
//#import "BIZPopupViewController.h"

#import "HomeCVC.h"
#import "CollectionViewCell.h"
#import "AppDelegate.h"
#import "PersistentStore.h"
#import "NSString+HTML.h"
#import "Language.h"
#import "MBProgressHUD.h"
#import "SubProjectRecord.h"
#import "StoresVC.h"
#import "NotesVC.h"
#import "SettingVC.h"
#import "RatingIndexVC.h"
#import "ContactUsTVC.h"
#import "Reachability.h"
#import "LogVC.h"
#import "ProjectReportTVC.h"
#import <SplunkMint/SplunkMint.h>

@interface StartProjectPopUpVC ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextViewDelegate>


@end

@implementation StartProjectPopUpVC

{
    NSMutableArray *projectTitleArray;
    NSMutableArray *projectSubTitleArray;
    NSMutableArray *projectExtIdArray;
     NSMutableArray *projectidArray;
    NSMutableArray *arrayLogTime;
    NSString *pickerType;
    NSString *projectIDIndex;
    int count;
    NSInteger indexRow;
    NSString *selectProjectIndex;
    NSString *selectSubProjectIndex;
    NSString *startTimeString;
    NSString *stopTimeString;
    
    NSString *currentDateString;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_commentTextView setDelegate:self];
    

    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int countValue = (int)[defaults integerForKey:@"count"];
    
    if (countValue==1) {
        
        _stopBackgroundView.hidden=NO;
        _timePicker.hidden=YES;
        [_stopBackgroundView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_main.png"]]];
        [self getTimeLogValuesURl];

        [self stopProjectView];
    }
    else
    {
        UIImageView *background = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [background setImage:[UIImage imageNamed:@"background_main.png"]];
        [self.view insertSubview:background atIndex:0];
        
        _stopBackgroundView.hidden=YES;
        _timePicker.hidden=YES;
        
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *currentDate = [dateFormatter stringFromDate: currentTime];
        currentDateString =currentDate;
        NSLog(@"curent date :%@",currentDateString);
        
        [self postRequestWorkersProjects];

        [self openPickerView];

    
    }
    pickerType=@"";
   

    
    NSLog(@"picker type 1 %@",pickerType);
    
    
    _tablePopupView.hidden=YES;
    _tableView.hidden=YES;

    
    
    _pickerBackView.hidden=YES;
   // _pickerView.hidden = YES;


    NSLog(@"start popup view is open ");
    
}




-(void)stopProjectView
{
    _stopProjectLabel.text = [Language get:@"Stop Project" alter:@"!Stop Project"];
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     NSString *startTime = [defaults objectForKey:@"currentTime"];
    _labelStartTime.text =startTime;
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm a"];
    NSString *resultString = [timeFormatter stringFromDate: currentTime];
    NSLog(@"current time get %@",resultString);
    _labelEndTime.text =resultString;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDate = [dateFormatter stringFromDate: currentTime];
    currentDateString =currentDate;
    _dateLabel.text =[NSString stringWithFormat:@"  %@", currentDate];
    
    _stopButton.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _stopButton.layer.borderWidth=1;
    [_stopButton setTitle:[Language get:@"Stop" alter:@"!Stop"] forState:UIControlStateNormal];

    _stopButton.layer.cornerRadius = 10;
    
    _labelStartTime.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _labelStartTime.layer.borderWidth=1;
     _labelStartTime.layer.cornerRadius = 5;
    
    _labelEndTime.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _labelEndTime.layer.borderWidth=1;
    _labelEndTime.layer.cornerRadius = 5;
    
    _dateLabel.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _dateLabel.layer.borderWidth=1;
    _dateLabel.layer.cornerRadius = 5;
    
    _modeLabel.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _modeLabel.layer.borderWidth=1;
    _modeLabel.layer.cornerRadius = 5;
    
    _commentTextView.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _commentTextView.layer.borderWidth=1;
    _commentTextView.layer.cornerRadius = 5;
    
    _pickerBackView.frame=CGRectMake(0, 150, self.view.frame.size.width  , 420);
    
    UITapGestureRecognizer * startTimeTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startTimeHandleSingleTap:)];
    startTimeTapRecognizer.numberOfTapsRequired = 1;
    _labelStartTime.userInteractionEnabled = YES;
    [_labelStartTime addGestureRecognizer:startTimeTapRecognizer];
    
    UITapGestureRecognizer * stopTimeTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopTimeHandleSingleTap:)];
     stopTimeTapRecognizer.numberOfTapsRequired = 1;
     _labelEndTime.userInteractionEnabled = YES;
    [_labelEndTime addGestureRecognizer:stopTimeTapRecognizer];
    
    
    UITapGestureRecognizer * timeLogTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(timeLogsHandleSingleTap:)];
    timeLogTapRecognizer.numberOfTapsRequired = 1;
    _modeLabel.userInteractionEnabled = YES;
    [_modeLabel addGestureRecognizer:timeLogTapRecognizer];
    
    UITapGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [_stopBackgroundView addGestureRecognizer:tapRecognizer];
    
    
    _timePicker.backgroundColor = [UIColor colorWithRed:0.87 green:0.88 blue:0.89 alpha:1.0];
    
}


- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    _timePicker.hidden=YES;
    _pickerBackView.hidden=YES;
   // _pickerView.hidden = YES;
    [self.view endEditing:YES];
}



- (void)timeLogsHandleSingleTap:(UITapGestureRecognizer *) sender
{
    pickerType=@"Time log";
    _cancelButton.hidden=YES;
    [_pickerView reloadAllComponents];
    _pickerBackView.hidden=NO;
    //_pickerView.hidden = NO;
    [self openPickerView];
}

- (void)startTimeHandleSingleTap:(UITapGestureRecognizer *) sender
{
    
    _timePicker.hidden=NO;
    _pickerBackView.hidden=YES;
   //  _pickerView.hidden = YES;
    _timePicker.tag=101;
    _timePicker.datePickerMode = UIDatePickerModeTime;
    [_timePicker addTarget:self action:@selector(ToStartTimeLabel:) forControlEvents:UIControlEventValueChanged];
    
}


-(void) ToStartTimeLabel:(id)sender
{
    

    NSString *stopLabelTime = _labelEndTime.text;
    NSLog(@"stop time is %@",stopLabelTime);

    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
     [timeFormat setDateFormat:@"HH:mm a"];
    
    NSDate *starteventTime = [_timePicker date];
    startTimeString = [timeFormat stringFromDate:starteventTime];
    
    if (_timePicker.tag==101) {
    
    NSComparisonResult result;
    result = [startTimeString compare:stopLabelTime]; // comparing two dates
    if(result==NSOrderedAscending) { // it checks like this LastDate > FirstDate
        NSLog(@"correct");

            _labelStartTime.text = [NSString stringWithFormat:@"%@",startTimeString];

    }
    else{
        NSLog(@"error");
    
     [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:@"Start time cannot greater then end Time!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

    }
        
    }
     _timePicker.hidden=YES;
}




- (void)stopTimeHandleSingleTap:(UITapGestureRecognizer *) sender
{
    _timePicker.hidden=NO;
    _pickerBackView.hidden=YES;
    // _pickerView.hidden = YES;
    _timePicker.tag=102;
    _timePicker.datePickerMode = UIDatePickerModeTime;
    [_timePicker addTarget:self action:@selector(ToStopTimeLabel:) forControlEvents:UIControlEventValueChanged];

}


-(void) ToStopTimeLabel:(id)sender
{

    NSString *startLabelTime = _labelStartTime.text;
    NSLog(@"start time is %@",startLabelTime);
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm a"];
    NSDate *stopEventTime = [_timePicker date];
    stopTimeString = [timeFormat stringFromDate:stopEventTime];
    
     if (_timePicker.tag==102) {
    NSComparisonResult result;
    result = [stopTimeString compare:startLabelTime]; // comparing two dates
    if(result==NSOrderedDescending) { // it checks like this LastDate > FirstDate
        NSLog(@"correct");

            _labelEndTime.text = [NSString stringWithFormat:@"%@",stopTimeString];
    }
    else{
        NSLog(@"error");
        
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:@"End time cannot greater then Start time!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

    }
     }

      _timePicker.hidden=YES;
}



-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading...";
    hud.dimBackground = YES;
}


-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _startProjectLabel.text = [Language get:@"Start Project" alter:@"!Start Project"];

    _subProjectTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _subProjectTF.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _subProjectTF.layer.borderWidth=1;
    _subProjectTF.placeholder=[Language get:@"  Select sub project" alter:@"!  Select sub project"];//
    _subProjectTF.layer.cornerRadius = 5;
    
    
    _workerProjectTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _workerProjectTF.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _workerProjectTF.layer.borderWidth=1;
    _workerProjectTF.placeholder = [Language get:@"  Select project name" alter:@"!  Select project name"];
    _workerProjectTF.layer.cornerRadius = 5;
    
    
    _startButton.layer.borderColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0].CGColor;
    _startButton.layer.borderWidth=1;
    [_startButton setTitle:[Language get:@"Start" alter:@"!Start"] forState:UIControlStateNormal];
    _startButton.layer.cornerRadius = 10;
    
    
    UIImage *image = [[UIImage imageNamed:@"ic_dropdown@3x.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *dropImage = [[UIImageView alloc] initWithImage:image];
    dropImage.frame = CGRectMake(0.0, 0.0, dropImage.image.size.width+35.0, dropImage.image.size.height);
    dropImage.contentMode = UIViewContentModeCenter;
    dropImage.tintColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0];
    _workerProjectTF.rightView = dropImage;
    _workerProjectTF.rightViewMode = UITextFieldViewModeAlways;
    
    
    UIImage *image1 = [[UIImage imageNamed:@"ic_dropdown@3x.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *dropImage1 = [[UIImageView alloc] initWithImage:image1];
    dropImage1.frame = CGRectMake(0.0, 0.0, dropImage1.image.size.width+35.0, dropImage1.image.size.height);
    dropImage1.contentMode = UIViewContentModeCenter;
    dropImage1.tintColor = [UIColor colorWithRed:0.05 green:0.48 blue:1.00 alpha:1.0];
    _subProjectTF.rightView = dropImage1;
    _subProjectTF.rightViewMode = UITextFieldViewModeAlways;
    

    
    
    //[self postRequestWorkersSubProjects:subProjectNameIndex];

}




- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)showWorkerProjectList:(id)sender {

    
    
    pickerType=@"projects";
    _cancelButton.hidden=YES;
     [_pickerView reloadAllComponents];
    _pickerBackView.hidden=NO;
   // _pickerView.hidden = NO;
//      [self openPickerView];
    
    NSLog(@"picker type 2 %@",pickerType);
}

- (IBAction)showProjectSubList:(id)sender {
    
    
    if (projectSubTitleArray.count>0) {
        _cancelButton.hidden=NO;
        pickerType=@"SubProjects";
          [_pickerView reloadAllComponents];
        _pickerBackView.hidden=NO;
       // _pickerView.hidden = NO;
        // [self openPickerView];

             }
    
    else{
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"No sub projects." alter:@"!No sub projects."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

    }
    
   
    NSLog(@"picker type 3 %@",pickerType);
}

- (IBAction)startButtonAction:(id)sender {
    
    
    if ([_workerProjectTF.text isEqualToString:@""]) {
        
        [[[UIAlertView alloc]initWithTitle:@"Empty!" message:@"Select project name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        
    }
    
    else{

    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm a"];
    NSString *startTimeStrings = [dateFormatter stringFromDate: currentTime];
    NSLog(@"current time get %@",startTimeStrings);

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    [defaults setObject:startTimeStrings forKey:@"currentTime"];
    [defaults synchronize];
    
    NSDate *datePlusOneMinute = [currentTime dateByAddingTimeInterval:60];
    NSDateFormatter *dateFormatterr = [[NSDateFormatter alloc] init];
    [dateFormatterr setDateFormat:@"HH:mm a"];
    NSString *stopTime = [dateFormatterr stringFromDate:datePlusOneMinute];
    
    
    NSLog(@"start time :%@",startTimeStrings);
    NSLog(@"stop time :%@",stopTime);
    NSLog(@"comment :%@",_commentTextView.text);
    NSLog(@"date:%@",currentDateString);
    
    
    [self startTimerPost:startTimeStrings Stop:stopTime comment:@"" date:currentDateString];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StartTimerAuthentication" object:self userInfo:nil];
        
       
    
   }
    
    
    
}


- (IBAction)stopButtonPressed:(id)sender {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm a"];
    NSString *stopTimeStrings = [dateFormatter stringFromDate: currentTime];
    NSLog(@"current time get %@",stopTimeStrings);

    
    NSString *startTime = [defaults objectForKey:@"currentTime"];
      [[NSNotificationCenter defaultCenter] postNotificationName:@"StopTimerAuthentication" object:self userInfo:nil];
    [self stopTimerPost:startTime Stop:_labelEndTime.text comment:_commentTextView.text date:_dateLabel.text];

  
 
}




-(void)startTimerPost:(NSString *)startTime Stop:(NSString *)stopTime comment:(NSString*)comnt date:(NSString*)datee
{
    
    [self showLoadingView];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *projectId = [defaults objectForKey:@"projectID"];
    
    NSString *projectExtId = @"";
    projectExtId = [defaults objectForKey:@"projectExtID"];

    
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    [postDict setObject:[PersistentStore getWorkerID] forKey:@"worker_id"];
    [postDict setObject:projectId forKey:@"project_id"];
    [postDict setObject:@"" forKey:@"external_project_id"];
    
    if (projectExtId !=nil) {
         [postDict setObject:projectExtId forKey:@"head_id"];
    }
    else{
        [postDict setObject:@"" forKey:@"head_id"];

    }

    [postDict setObject:stopTime forKey:@"is_to"];
    [postDict setObject:startTime forKey:@"is_from"];
    [postDict setObject:datee forKey:@"date"];
    
    [postDict setObject:@"Normal time" forKey:@"priority"];
    [postDict setObject:comnt forKey:@"comment"];
    [postDict setObject:@"0" forKey:@"is_vehicle_used"];
    
    
    NSLog(@"start post values is :%@",postDict);
    
    if (APP_DELEGATE.isServerReachable) {
        
        NSMutableURLRequest *urlRequest;
        
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        
        if ([_subProjectTF.text isEqualToString:@""] && ![_workerProjectTF.text isEqualToString:@""]) {
            urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_START_PROJECT]];
            
            NSLog(@"worker selected & sub empty");
        }
        else {
        urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_TIME_LOG_HEAD]];
                        NSLog(@"worker selected & sub selected");
            
        }
        
        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             NSLog(@"response on start button**%@",object);
             if (error)
             {
                 NSLog(@"Error: %@",[error description]);
             }
             if ([object isKindOfClass:[NSDictionary class]] == YES)
             {
                 if ([object[@"CODE"] intValue]==1)
                 {
                     [[[UIAlertView alloc] initWithTitle:@"Projects" message:[Language get:@"No Projects yet." alter:@"!No Projects yet."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                     
                     // [self performSelectorOnMainThread:@selector(alertLoginFailed) withObject:nil waitUntilDone:YES];
                 }
                 else
                 {
                    NSMutableArray *objEvents=object[@"data"];
                 //   NSMutableArray *workerLoginId = [@[] mutableCopy];
                     
                     NSString *e_worker_logid =object[@"data"][@"e_worker_logid"];
//                     NSMutableArray *projectTitles =[@[] mutableCopy];
//                     NSMutableArray *projectIds =[@[] mutableCopy];
//                     for(NSMutableDictionary *objEvent in objEvents)
//                     {
//                       //  @autoreleasepool
//                        // {
//                             
//                             
//                            [workerLoginId addObject:objEvent[@"e_worker_logid"]];
//                             NSLog(@"e_worker_loginId is :%@",workerLoginId);
////                             ProjectRecord *project=[[ProjectRecord alloc] init];
////                             project.projectId=objEvent[@"project_id"];
////                             project.projectExternalId=objEvent[@"external_project_id"];
////                             project.location = objEvent[@"location"];
////                             project.city = objEvent[@"city"];
////                             project.state = objEvent[@"state"];
////                             project.Country = objEvent[@"country"];
////                             project.projectName=objEvent[@"title"];
////                             project.projectImageURL=[NSURL URLWithString:objEvent[@"file_name"]];
////                             [records addObject:project];
////                          //   [projectTitles addObject:project.projectName];
////                         //    [projectIds addObject:project.projectId];
////                             project=nil;
//                        // }
//                         
//                     }
                     count=1;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:e_worker_logid forKey:@"e_workerLoginID"];
                     [defaults setInteger:count forKey:@"count"];
                     [defaults synchronize];

                     NSLog(@"E WORKER ID*** :%@",e_worker_logid);
                     
                     
                     
                     
                     
                     
                      [self hideLoadingView];
                      [self dismissViewControllerAnimated:YES completion:nil];
                 }
             }
             
         }];
    }
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
    
}







-(void)stopTimerPost:(NSString *)startTime Stop:(NSString *)stopTime comment:(NSString*)comnt date:(NSString*)datee
{

    [self showLoadingView];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *projectId = [defaults objectForKey:@"projectID"];
    NSString *projectExtId = @"";
    projectExtId = [defaults objectForKey:@"projectExtID"];
    NSString *e_WorkerID = [defaults objectForKey:@"e_workerLoginID"];
    
    NSLog(@"project id *****%@",projectId);
    
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    [postDict setObject:[PersistentStore getWorkerID] forKey:@"worker_id"];
    [postDict setObject:projectId forKey:@"pname"];
    [postDict setObject:@"" forKey:@"external_project_id"];
    
    if (projectExtId !=nil) {
    [postDict setObject:projectExtId forKey:@"head_id"];
    }
    
    else{
        [postDict setObject:@"" forKey:@"head_id"];

    }
    
    [postDict setObject:stopTime forKey:@"is_to"];
    [postDict setObject:startTime forKey:@"is_from"];
    [postDict setObject:datee forKey:@"date"];
    
    [postDict setObject:@"Normal time" forKey:@"priority"];
    [postDict setObject:comnt forKey:@"comment"];
    [postDict setObject:@"0" forKey:@"is_vehicle_used"];
   [postDict setObject:e_WorkerID forKey:@"e_worker_logid"];
    
    
    NSLog(@"Values on stop time action :%@",postDict);
    
    if (APP_DELEGATE.isServerReachable) {
        
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_STOP_PROJECT]];
        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             NSLog(@" stop response :%@",object);
             if (error)
             {
                 NSLog(@"Error: %@",[error description]);
             }
             if ([object isKindOfClass:[NSDictionary class]] == YES)
             {
                 if ([object[@"CODE"] intValue]==1)
                 {
                     [[[UIAlertView alloc] initWithTitle:@"Projects" message:[Language get:@"No Projects yet." alter:@"!No Projects yet."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                     
                     // [self performSelectorOnMainThread:@selector(alertLoginFailed) withObject:nil waitUntilDone:YES];
                 }
                 else
                 {
        // code here
                     
                     count=0;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setInteger:count forKey:@"count"];
                     [defaults synchronize];
                     
                     [self hideLoadingView];

                     [self dismissViewControllerAnimated:YES completion:nil];

                 }
             }
             
         }];
    }
    else
    {
                [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
    
}




-(void)getTimeLogValuesURl
{
    
    [self showLoadingView];

    
    arrayLogTime=[[NSMutableArray alloc] init];
    
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    [postDict setObject:[PersistentStore getLocalLanguage] forKey:@"lang"];
    
    
    // {"worker_id":"18","project_id":"126","is_to":"09:30","is_from":"10:30","comment":"zyx","date":"2013-11-29","priority":"Uncomfortable time"}';
    if (APP_DELEGATE.isServerReachable) {
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_TIME_LOG_TYPE_OPTIONS]];
        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             NSLog(@" time code -----------%@",object);
             if (!error)
             {
                 NSMutableArray *typeArray=object;
                 
                 for (NSDictionary *logs in typeArray) {
                     NSString *valueString = logs[@"value"];
                     [arrayLogTime addObject:[valueString stringByConvertingHTMLToPlainText]];
                 }
                 
                 NSLog(@"time logs values %@",arrayLogTime);
                  [self performSelectorOnMainThread:@selector(hide) withObject:nil waitUntilDone:YES];
                // [self performSelectorOnMainThread:@selector(ReloadDataOfTable) withObject:nil waitUntilDone:YES];
             }
         }];
    }
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }

    
    
}













-(void)postRequestWorkersProjects
{
    [self showLoadingView];
    
    self.arrayProjects = [[NSMutableArray alloc]init];
    projectTitleArray = [[NSMutableArray alloc]init];
    projectidArray = [[NSMutableArray alloc]init];
    projectExtIdArray = [[NSMutableArray alloc]init];
    
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    [postDict setObject:[PersistentStore getWorkerID] forKey:@"worker_id"];
    
    if (APP_DELEGATE.isServerReachable) {
        
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_RECENTPROJECTS_LIST]];
        
        //URL_PROJECTS_LIST
        
        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             NSLog(@"  projects list----%@",object);
             if (error)
             {
                 NSLog(@"Error: %@",[error description]);
             }
             if ([object isKindOfClass:[NSDictionary class]] == YES)
             {
                 if ([object[@"CODE"] intValue]==1)
                 {
                     [[[UIAlertView alloc] initWithTitle:@"Projects" message:[Language get:@"No Projects yet." alter:@"!No Projects yet."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                     
                     // [self performSelectorOnMainThread:@selector(alertLoginFailed) withObject:nil waitUntilDone:YES];
                 }
                 else
                 {
                     NSMutableArray *objEvents=object[@"data"];
                     NSMutableArray *records = [@[] mutableCopy];
                     NSMutableArray *projectTitles =[@[] mutableCopy];
                     NSMutableArray *projectIds =[@[] mutableCopy];
                     NSMutableArray *projectExt_id =[@[] mutableCopy];
                     for(NSMutableDictionary *objEvent in objEvents)
                     {
                         @autoreleasepool
                         {
                             ProjectRecord *project=[[ProjectRecord alloc] init];
                             project.projectId=objEvent[@"project_id"];
                             project.projectExternalId=objEvent[@"external_project_id"];
                             project.location = objEvent[@"location"];
                             project.city = objEvent[@"city"];
                             project.state = objEvent[@"state"];
                             project.Country = objEvent[@"country"];
                             project.projectName=objEvent[@"title"];
                             project.projectImageURL=[NSURL URLWithString:objEvent[@"file_name"]];
                             [records addObject:project];
                             [projectTitles addObject:[project.projectName stringByConvertingHTMLToPlainText]];
                             [projectIds addObject:project.projectId];
                             //[projectExt_id addObject:project.projectExternalId];

                             project=nil;
                         }
                         
                     }
                     
                     
                     
                     projectTitleArray =projectTitles;
                    // projectExtIdArray =projectExt_id;
                     NSLog(@"projects titles@@@@  %@",projectTitleArray);
                     projectidArray =projectIds;
                     //self.arrayProjects=records;
                     
                     indexRow = [_pickerView selectedRowInComponent:0];
                     selectProjectIndex = [projectTitleArray objectAtIndex:indexRow];
                     selectSubProjectIndex= [projectidArray objectAtIndex:indexRow];
                     NSLog(@"current picker value [%@] %@",selectSubProjectIndex,selectProjectIndex);
                     
                     dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                         //Background Thread
                         
                         
                         dispatch_async(dispatch_get_main_queue(), ^(void){
                             //Run UI Updates
                             
                             
                    _workerProjectTF.text = [NSString stringWithFormat:@"[%@] %@",selectSubProjectIndex,selectProjectIndex];
                         });
                     });
                     
                     
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:selectProjectIndex forKey:@"projectNameIndex"];
                    // [defaults setObject:selectSubProjectIndex forKey:@"SubprojectNameIndex"];
                     [defaults setObject:selectSubProjectIndex forKey:@"projectID"];
                     NSLog(@"subProjectID is %@",selectSubProjectIndex);
                     [defaults synchronize];
                     
                     [self postRequestWorkersSubProjects:selectSubProjectIndex selectSub:NO];
                     [self performSelectorOnMainThread:@selector(hideLoadingView) withObject:nil waitUntilDone:YES];

                 }
             }
             
         }];
    }
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}












-(void)openPickerView
{
    
    _pickerView.delegate =self;
    _pickerView.dataSource =self;
    [_pickerView reloadAllComponents];
    [_pickerBackView addSubview:_pickerView];
    [self.view addSubview:_pickerBackView];
 //   [self .viewIfLoaded addSubview:_pickerView];
    
}
- (IBAction)doneButtonAction:(id)sender {
    
   
  
    
    
    if ([pickerType isEqualToString:@"SubProjects"]){
        NSString *YourselectedTitle = [projectSubTitleArray objectAtIndex:[_pickerView selectedRowInComponent:0]];//
        _subProjectTF.text = YourselectedTitle;
    }
      _pickerBackView.hidden=YES;
     pickerType=@"";
}

- (IBAction)cancelButtonAction:(id)sender {
    
    
    _pickerBackView.hidden=YES;
    pickerType=@"";
    _subProjectTF.text= nil;
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    NSLog(@"picker type 4 %@",pickerType);
     if ([pickerType isEqualToString:@"projects"]) {
         
      NSString *YourselectedTitle = [NSString stringWithFormat:@"[%@] %@",[projectidArray objectAtIndex:[_pickerView selectedRowInComponent:0]],[projectTitleArray objectAtIndex:[_pickerView selectedRowInComponent:0]]];
         
       _workerProjectTF.text = YourselectedTitle;
       NSLog(@"update : %@",YourselectedTitle);

         NSString *projectIDIndexs = [projectidArray objectAtIndex:row];
         [self postRequestWorkersSubProjects:projectIDIndexs selectSub:YES];
         
         _subProjectTF.text=@"";

         
         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
         [defaults setObject:projectIDIndexs forKey:@"projectID"];
         //[defaults setObject:projectExt_ID forKey:@"projectExtID"];
         [defaults synchronize];
         
         //_pickerView.hidden=YES;
     }
    
     else if ([pickerType isEqualToString:@"SubProjects"]){
         NSString *YourselectedTitle = [projectSubTitleArray objectAtIndex:[_pickerView selectedRowInComponent:0]];//
         _subProjectTF.text = YourselectedTitle;
         
          NSString *YoursubProjectHeadId = [projectExtIdArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
          NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
           [defaults setObject:YoursubProjectHeadId forKey:@"projectExtID"];
          [defaults synchronize];
        //  _pickerView.hidden=YES;
          //pickerType=@"";
     }
    
     else if ([pickerType isEqualToString:@"Time log"]){
         NSString *YourselectedTitle = [arrayLogTime objectAtIndex:[_pickerView selectedRowInComponent:0]];
         _modeLabel.text =[NSString stringWithFormat:@"  %@", YourselectedTitle];
        // _pickerView.hidden=YES;
        // pickerType=@"";
     }
    NSLog(@"picker type 5 %@",pickerType);
    
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if ([pickerType isEqualToString:@"projects"]) {
        return projectTitleArray.count;

    }
    else if ([pickerType isEqualToString:@"SubProjects"]){
        return projectSubTitleArray.count;

    }
    else{
        return arrayLogTime.count;

    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    if ([pickerType isEqualToString:@"projects"]) {
    title = [NSString stringWithFormat:@"[%@] %@",[projectidArray objectAtIndex:row] ,[projectTitleArray objectAtIndex:row]];
    }
    else if ([pickerType isEqualToString:@"SubProjects"])
    {
        title = [NSString stringWithFormat:@"%@",[projectSubTitleArray objectAtIndex:row]];
    }
    
    else if ([pickerType isEqualToString:@"Time log"])
    {
        title = [NSString stringWithFormat:@"%@",[arrayLogTime objectAtIndex:row]];
    }
    return title;
}


- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}




- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=3;
    }
    // Fill the label text here
    //tView.text=[wishvalues objectAtIndex:row];
    
    if ([pickerType isEqualToString:@"projects"]) {
        tView.text = [NSString stringWithFormat:@"[%@] %@",[projectidArray objectAtIndex:row] ,[projectTitleArray objectAtIndex:row]];
    }
    else if ([pickerType isEqualToString:@"SubProjects"])
    {
        tView.text = [NSString stringWithFormat:@"%@",[projectSubTitleArray objectAtIndex:row]];
    }
    
    else if ([pickerType isEqualToString:@"Time log"])
    {
        tView.text = [NSString stringWithFormat:@"%@",[arrayLogTime objectAtIndex:row]];
    }
    
    
    return tView;
}








-(void)postRequestWorkersSubProjects:(NSString*)projectId  selectSub:(BOOL)select
{
    if (select==YES) {
        [self showLoadingView];

    }
    
    
    projectSubTitleArray = [[NSMutableArray alloc]init];
    
    NSError *error;
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc] init];
    [postDict setObject:projectId forKey:@"project_id"];
    
    if (APP_DELEGATE.isServerReachable) {
        
        NSData *jsonData= [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_SUBPROJECTS_LIST]];
        [urlRequest setTimeoutInterval:180];
        NSString *requestBody = [NSString stringWithFormat:@"JsonObject=%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             NSLog(@"sub projects list----%@",object);
             if (error)
             {
                 NSLog(@"Error: %@",[error description]);
             }
             if ([object isKindOfClass:[NSDictionary class]] == YES)
             {
//                 if ([object[@"CODE"] intValue]==1)
//                 {
//                     [[[UIAlertView alloc] initWithTitle:@"Projects" message:[Language get:@"No Projects yet." alter:@"!No Projects yet."]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//                     
//                     // [self performSelectorOnMainThread:@selector(alertLoginFailed) withObject:nil waitUntilDone:YES];
//                 }
//                 else
//                 {
                     NSMutableArray *objEvents=object[@"data"];
                     NSMutableArray *records = [@[] mutableCopy];
                     NSMutableArray *subProjects = [@[] mutableCopy];
                     NSMutableArray *head_id = [@[] mutableCopy];
                     SubProjectRecord* project=[[SubProjectRecord alloc] init];
                     project.projectId=(objEvents.count)?objEvents[0][@"project_id"]:@"0";
                     project.headId=@"0";
                     project.headName=[Language get:@"Select sub project(Optional)" alter:@"!Select sub project(Optional)"] ;
                     [records addObject:project];
                     
                     for(NSMutableDictionary *objEvent in objEvents)
                     {
                         @autoreleasepool
                         {
                             SubProjectRecord* project=[[SubProjectRecord alloc] init];
                             project.projectId=objEvent[@"project_id"];
                             project.headId=objEvent[@"head_id"];
                             
                             
                             project.headName=objEvent[@"head_name"];
                             
                             [head_id addObject:project.headId];
                             [records addObject:project];
                             [subProjects addObject:[project.headName stringByConvertingHTMLToPlainText]];
                             project=nil;
                         }
                         
                     }
                     
                     projectSubTitleArray =subProjects;
                     projectExtIdArray =head_id;
                 
                 if (projectExtIdArray.count==0) {
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:@"" forKey:@"projectExtID"];
                     [defaults synchronize];

                 }
                 
               
                  //   _subProjectTF.text = [NSString stringWithFormat:@"%@",projectSubTitleArray];
                     NSLog(@"sub projects list %@",projectSubTitleArray);
                     //self.arraySubProjects=records;
                     
                     [self performSelectorOnMainThread:@selector(hide) withObject:nil waitUntilDone:YES];
                     
                 //}
             }
             
         }];
    }
    else
    {
        [self hideLoadingView];
        [[[UIAlertView alloc]initWithTitle:@"Fieldo" message:[Language get:@"Internet connection is not available. Please try again." alter:@"!Internet connection is not available. Please try again."]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}


-(void)hide
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}



//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event  {
//    NSLog(@"touches began");
//    UITouch *touch = [touches anyObject];
//    if(touch.view!=_pickerView){
//        _pickerBackView.hidden=YES;
//        _pickerView.hidden = YES;
//    } 
//}



-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _commntPlaceHolder.hidden = YES;
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([_commentTextView.text isEqualToString:@""]) {
        _commntPlaceHolder.hidden = NO;
        
    }else{
        _commntPlaceHolder.hidden = YES;
    }
}


-(void)textViewDidChangeSelection:(UITextView *)textView
{
    if ([_commentTextView.text isEqualToString:@""]) {
        _commntPlaceHolder.hidden = NO;
        
    }else{
        _commntPlaceHolder.hidden = YES;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}



@end
