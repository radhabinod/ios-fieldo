//
//  PopUpVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 15/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface StartProjectPopUpVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *workerProjectTF;
@property (weak, nonatomic) IBOutlet UITextField *subProjectTF;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UILabel *startProjectLabel;

@property (weak, nonatomic) IBOutlet UIView *pickerBackView;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *arrayProjects;
@property (nonatomic,strong) NSMutableArray *arraySubProjects;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;


@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *tablePopupView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (weak, nonatomic) IBOutlet UIView *stopBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *labelStartTime;
@property (weak, nonatomic) IBOutlet UILabel *labelEndTime;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;
@property (weak, nonatomic) IBOutlet UILabel *stopProjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *commntPlaceHolder;


@end
