//
//  SubProjectRecord.h
//  Fieldo
//
//  Created by Macbook on 7/21/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubProjectRecord : NSObject

@property(nonatomic,strong) NSString *projectId;
@property(nonatomic,strong) NSString *headName;
@property(nonatomic,strong) NSString *headId;

@end
