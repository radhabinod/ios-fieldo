//
//  TimeLogVC.h
//  Fieldo
//
//  Created by Gagan Joshi on 11/11/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLogVC : UITableViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate>

@property(nonatomic,retain)  NSString *stringProjectId;
@property(nonatomic,retain)  NSString *stringProjectExtId;
@property(nonatomic,retain)  NSString *stringLocation;
@property(nonatomic,retain)  NSString *stringCity;
@property(nonatomic,retain)  NSString *stringState;
@property(nonatomic,retain)  NSString *stringCountry;
@property(nonatomic,retain)  NSString *stringProjectName;
@property(nonatomic,retain)  NSString *stringHeadId;
@property(nonatomic,retain)  NSString *taskName;

@property (nonatomic,retain) NSMutableArray *arrayLogTime;
@property (assign,nonatomic) BOOL shouldfromComment;

// Mandeep
// Edit Log
@property(nonatomic,retain)  NSString *e_worker_logid;
@property(nonatomic,retain)  NSString *is_from;
@property(nonatomic,retain)  NSString *is_to;
@property(nonatomic,retain)  NSString *is_vehicle_used;
@property(nonatomic,retain)  NSString *date;
@property(nonatomic,retain)  NSString *tdiff;
@property(nonatomic,retain)  NSString *comment;


@end
